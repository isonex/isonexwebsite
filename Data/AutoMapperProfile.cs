using AutoMapper;
using isonexwebsite.Data.Entities;
using isonexwebsite.Models.ContactModels;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // CreateMap<User, UserDto>();
            // CreateMap<UserDto, User>();
            // CreateMap<User, UserViewModel>();

            CreateMap<Contact, ContactDto>();
            CreateMap<ContactDto, Contact>();

            
        }
    }
}