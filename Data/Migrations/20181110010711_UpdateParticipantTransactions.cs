﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Data.Migrations
{
    public partial class UpdateParticipantTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantBitcoinTransactions_BitcoinAddress_AddressId",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantEthereumTransactions_EthereumAddress_AddressId",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropIndex(
                name: "IX_ParticipantEthereumTransactions_AddressId",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropIndex(
                name: "IX_ParticipantBitcoinTransactions_AddressId",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "DateMined",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "RewardedTokens",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "Transaction",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "TransferedToFund",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "DateMined",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "RewardedTokens",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "Transaction",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "TransferedToFund",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.AddColumn<long>(
                name: "BlockNumber",
                table: "ParticipantEthereumTransactions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateBlockMined",
                table: "ParticipantEthereumTransactions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DepositedAmount",
                table: "ParticipantEthereumTransactions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "DepositingAddressId",
                table: "ParticipantEthereumTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "EthUsdPriceAtDateBlockMined",
                table: "ParticipantEthereumTransactions",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<bool>(
                name: "EtherTransferedToFund",
                table: "ParticipantEthereumTransactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "FromAddress",
                table: "ParticipantEthereumTransactions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Ix15TokensTransferedToParticipant",
                table: "ParticipantEthereumTransactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "Ix15UsdStagePrice",
                table: "ParticipantEthereumTransactions",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "NumberOfIx15TokensAssigned",
                table: "ParticipantEthereumTransactions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "TokenSaleStage",
                table: "ParticipantEthereumTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TransactionHash",
                table: "ParticipantEthereumTransactions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "BlockNumber",
                table: "ParticipantBitcoinTransactions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<bool>(
                name: "BtcTransferedToFund",
                table: "ParticipantBitcoinTransactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "BtcUsdPriceAtDateBlockMined",
                table: "ParticipantBitcoinTransactions",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateBlockMined",
                table: "ParticipantBitcoinTransactions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DepositedAmount",
                table: "ParticipantBitcoinTransactions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "DepositingAddressId",
                table: "ParticipantBitcoinTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "FromAddress",
                table: "ParticipantBitcoinTransactions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Ix15TokensTransferedToParticipant",
                table: "ParticipantBitcoinTransactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "Ix15UsdStagePrice",
                table: "ParticipantBitcoinTransactions",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "NumberOfIx15TokensAssigned",
                table: "ParticipantBitcoinTransactions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<int>(
                name: "TokenSaleStage",
                table: "ParticipantBitcoinTransactions",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "TransactionHash",
                table: "ParticipantBitcoinTransactions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantEthereumTransactions_DepositingAddressId",
                table: "ParticipantEthereumTransactions",
                column: "DepositingAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantBitcoinTransactions_DepositingAddressId",
                table: "ParticipantBitcoinTransactions",
                column: "DepositingAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantBitcoinTransactions_EthereumAddress_DepositingAddressId",
                table: "ParticipantBitcoinTransactions",
                column: "DepositingAddressId",
                principalTable: "EthereumAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantEthereumTransactions_EthereumAddress_DepositingAddressId",
                table: "ParticipantEthereumTransactions",
                column: "DepositingAddressId",
                principalTable: "EthereumAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantBitcoinTransactions_EthereumAddress_DepositingAddressId",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropForeignKey(
                name: "FK_ParticipantEthereumTransactions_EthereumAddress_DepositingAddressId",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropIndex(
                name: "IX_ParticipantEthereumTransactions_DepositingAddressId",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropIndex(
                name: "IX_ParticipantBitcoinTransactions_DepositingAddressId",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "BlockNumber",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "DateBlockMined",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "DepositedAmount",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "DepositingAddressId",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "EthUsdPriceAtDateBlockMined",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "EtherTransferedToFund",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "FromAddress",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "Ix15TokensTransferedToParticipant",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "Ix15UsdStagePrice",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "NumberOfIx15TokensAssigned",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "TokenSaleStage",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "TransactionHash",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "BlockNumber",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "BtcTransferedToFund",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "BtcUsdPriceAtDateBlockMined",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "DateBlockMined",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "DepositedAmount",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "DepositingAddressId",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "FromAddress",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "Ix15TokensTransferedToParticipant",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "Ix15UsdStagePrice",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "NumberOfIx15TokensAssigned",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "TokenSaleStage",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "TransactionHash",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "ParticipantEthereumTransactions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "ParticipantEthereumTransactions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateMined",
                table: "ParticipantEthereumTransactions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "ParticipantEthereumTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "RewardedTokens",
                table: "ParticipantEthereumTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Transaction",
                table: "ParticipantEthereumTransactions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TransferedToFund",
                table: "ParticipantEthereumTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Value",
                table: "ParticipantEthereumTransactions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "ParticipantBitcoinTransactions",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "ParticipantBitcoinTransactions",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateMined",
                table: "ParticipantBitcoinTransactions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "ParticipantBitcoinTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "RewardedTokens",
                table: "ParticipantBitcoinTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Transaction",
                table: "ParticipantBitcoinTransactions",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TransferedToFund",
                table: "ParticipantBitcoinTransactions",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Value",
                table: "ParticipantBitcoinTransactions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantEthereumTransactions_AddressId",
                table: "ParticipantEthereumTransactions",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantBitcoinTransactions_AddressId",
                table: "ParticipantBitcoinTransactions",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantBitcoinTransactions_BitcoinAddress_AddressId",
                table: "ParticipantBitcoinTransactions",
                column: "AddressId",
                principalTable: "BitcoinAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ParticipantEthereumTransactions_EthereumAddress_AddressId",
                table: "ParticipantEthereumTransactions",
                column: "AddressId",
                principalTable: "EthereumAddress",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
