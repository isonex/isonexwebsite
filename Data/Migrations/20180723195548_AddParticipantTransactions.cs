﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Data.Migrations
{
    public partial class AddParticipantTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ParticipantBitcoinTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    Processed = table.Column<bool>(type: "bit", nullable: false),
                    Transaction = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TransferedToFund = table.Column<bool>(type: "bit", nullable: false),
                    Value = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipantBitcoinTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ParticipantBitcoinTransactions_BitcoinAddress_AddressId",
                        column: x => x.AddressId,
                        principalTable: "BitcoinAddress",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ParticipantEthereumTransactions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    RewardedTokens = table.Column<bool>(type: "bit", nullable: false),
                    Transaction = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TransferedToFund = table.Column<bool>(type: "bit", nullable: false),
                    Value = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParticipantEthereumTransactions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ParticipantEthereumTransactions_EthereumAddress_AddressId",
                        column: x => x.AddressId,
                        principalTable: "EthereumAddress",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantBitcoinTransactions_AddressId",
                table: "ParticipantBitcoinTransactions",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_ParticipantEthereumTransactions_AddressId",
                table: "ParticipantEthereumTransactions",
                column: "AddressId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ParticipantBitcoinTransactions");

            migrationBuilder.DropTable(
                name: "ParticipantEthereumTransactions");
        }
    }
}
