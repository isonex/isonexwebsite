﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Data.Migrations
{
    public partial class MailTrainId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Subscriptions_MailChimpId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "MailChimpId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "MailChimpUniqueEmailAddress",
                table: "Subscriptions");

            migrationBuilder.AddColumn<string>(
                name: "MailTrainId",
                table: "Subscriptions",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MailTrainId",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_MailTrainId",
                table: "Subscriptions",
                column: "MailTrainId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Subscriptions_MailTrainId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "MailTrainId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "MailTrainId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "MailChimpId",
                table: "Subscriptions",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MailChimpUniqueEmailAddress",
                table: "Subscriptions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_MailChimpId",
                table: "Subscriptions",
                column: "MailChimpId",
                unique: true,
                filter: "[MailChimpId] IS NOT NULL");
        }
    }
}
