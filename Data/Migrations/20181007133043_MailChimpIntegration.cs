﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Data.Migrations
{
    public partial class MailChimpIntegration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MailChimpId",
                table: "Subscriptions",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MailChimpUniqueEmailAddress",
                table: "Subscriptions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_MailChimpId",
                table: "Subscriptions",
                column: "MailChimpId",
                unique: true,
                filter: "[MailChimpId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Subscriptions_MailChimpId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "MailChimpId",
                table: "Subscriptions");

            migrationBuilder.DropColumn(
                name: "MailChimpUniqueEmailAddress",
                table: "Subscriptions");
        }
    }
}
