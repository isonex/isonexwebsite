﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Data.Migrations
{
    public partial class AddDateMinedToTransactions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Processed",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateMined",
                table: "ParticipantEthereumTransactions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateMined",
                table: "ParticipantBitcoinTransactions",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "RewardedTokens",
                table: "ParticipantBitcoinTransactions",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateMined",
                table: "ParticipantEthereumTransactions");

            migrationBuilder.DropColumn(
                name: "DateMined",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.DropColumn(
                name: "RewardedTokens",
                table: "ParticipantBitcoinTransactions");

            migrationBuilder.AddColumn<bool>(
                name: "Processed",
                table: "ParticipantBitcoinTransactions",
                nullable: false,
                defaultValue: false);
        }
    }
}
