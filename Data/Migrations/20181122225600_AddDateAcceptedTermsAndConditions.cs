﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Data.Migrations
{
    public partial class AddDateAcceptedTermsAndConditions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "KYCPendingVerification",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "KYCVerified",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateAcceptedTermsAndConditions",
                table: "AspNetUsers",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "KycStatus",
                table: "AspNetUsers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateAcceptedTermsAndConditions",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "KycStatus",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<bool>(
                name: "KYCPendingVerification",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "KYCVerified",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);
        }
    }
}
