﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using IdentityDemo.Models;
using isonexwebsite.Data.Entities;
using IdentityDemo.Data.Entities;

namespace IdentityDemo.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Contact>()
                .Property(c => c.Timestamp)
                .HasDefaultValueSql("getdate()");

            builder.Entity<Subscription>()
                .Property(c => c.Timestamp)
                .HasDefaultValueSql("getdate()");

            builder.Entity<Subscription>()
                .HasIndex(u => u.MailTrainId);

            builder.Entity<EthereumAddress>()
                .Property(c => c.WhenCreated)
                .HasDefaultValueSql("getdate()");
            
            builder.Entity<BitcoinAddress>()
                .Property(c => c.WhenCreated)
                .HasDefaultValueSql("getdate()");

            builder.Entity<EthereumBlocksProcessed>()
                .Property(c => c.DateProcessed)
                .HasDefaultValueSql("getdate()");

            builder.Entity<EthereumAddress>()
                .Property(b => b.UserId)
                .HasMaxLength(450);

            builder.Entity<BitcoinAddress>()
                .Property(b => b.UserId)
                .HasMaxLength(450);

            builder.Entity<DbLogEntry>()
                .Property(p => p.Timestamp)
                .HasDefaultValueSql("getdate()");

            builder.Entity<DbLogEntry>()
                .Property(p => p.EntryType)
                .HasMaxLength(10);

            builder.Entity<ApplicationUser>()
                .Property(p => p.Created)
                .HasDefaultValueSql("getdate()");

        }

        public DbSet<EthereumAddress> EthereumAddress { get; set; }

        public DbSet<BitcoinAddress> BitcoinAddress { get; set; }

        public DbSet<PersonalEthereumAddress> PersonalEthereumAddress { get; set; }

        public DbSet<ParticipantBitcoinTransactions> ParticipantBitcoinTransactions { get; set; }

        public DbSet<ParticipantEthereumTransactions> ParticipantEthereumTransactions { get; set; }

        public DbSet<Contact> Contacts { get; set;}

        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<GlobalConfig> GlobalConfigs { get; set;}

        public DbSet<CrixDailyPriceHistory> CrixDailyPriceHistory { get; set; }

        public DbSet<IX15DailyNavPriceHistory> IX15DailyNavPriceHistory { get; set; }

        public DbSet<TeamMember> TeamMembers { get; set; }

        public DbSet<DbLogEntry> DbLogEntries { get; set; }

    }
}
