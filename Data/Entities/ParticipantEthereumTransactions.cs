﻿using System.ComponentModel.DataAnnotations.Schema;
using isonexwebsite.Data.Entities;
using System;

namespace IdentityDemo.Data.Entities
{
    public class ParticipantEthereumTransactions
    {
        public int Id { get; set; }
        public string TransactionHash { get; set; }
        public string FromAddress { get; set; }
        public int DepositingAddressId { get; set; }
        [ForeignKey("DepositingAddressId")]
        public EthereumAddress DepositingAddress { get; set; }
        public Int64 DepositedAmount { get; set; }
        public DateTime? DateBlockMined { get; set; }
        public long BlockNumber { get; set; }

        public int TokenSaleStage { get; set; }
        public decimal EthUsdPriceAtDateBlockMined { get; set; }
        public decimal Ix15UsdStagePrice { get; set; }
        public long NumberOfIx15TokensAssigned { get; set; }

        public bool Ix15TokensTransferedToParticipant { get; set; }
        public bool EtherTransferedToFund { get; set; }
    }

    public enum TokenSaleStage
    {
        TokenSaleHasntStarted,
        PrivatePreSale,
        PublicPreStage1,
        TokenSaleStage1,
        TokenSaleStage2,
        TokenSaleStage3,
        TokenSaleEnded,
    }
}