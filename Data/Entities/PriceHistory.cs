﻿using System;

namespace IdentityDemo.Data.Entities
{
    public class PriceHistory
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }
    }

    public class CrixDailyPriceHistory : PriceHistory
    {
    }

    public class IX15DailyNavPriceHistory : PriceHistory
    {
    }
}
