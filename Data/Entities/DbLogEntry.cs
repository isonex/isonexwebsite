﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityDemo.Data.Entities
{
    public class DbLogEntry
    {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int EntryLevel { get; set; }

        public Guid? CorrelationId { get; set; }
        public string EntryType { get; set; }

        public string Text { get; set; }

    }
}
