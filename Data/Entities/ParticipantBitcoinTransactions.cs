﻿using isonexwebsite.Data.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IdentityDemo.Data.Entities
{
    public class ParticipantBitcoinTransactions
    {
        public int Id { get; set; }
        public string TransactionHash { get; set; }
        public string FromAddress { get; set; }
        public int DepositingAddressId { get; set; }
        [ForeignKey("DepositingAddressId")]
        public EthereumAddress DepositingAddress { get; set; }
        public Int64 DepositedAmount { get; set; }
        public DateTime? DateBlockMined { get; set; }
        public long BlockNumber { get; set; }

        public int TokenSaleStage { get; set; }
        public double BtcUsdPriceAtDateBlockMined { get; set; }
        public double Ix15UsdStagePrice { get; set; }
        public long NumberOfIx15TokensAssigned { get; set; }

        public bool Ix15TokensTransferedToParticipant { get; set; }
        public bool BtcTransferedToFund { get; set; }
    }
}
