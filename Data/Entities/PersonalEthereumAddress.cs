﻿namespace IdentityDemo.Data.Entities
{
    public class PersonalEthereumAddress
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public string Address { get; set; }
    }
}
