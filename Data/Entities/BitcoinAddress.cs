using System;

namespace isonexwebsite.Data.Entities
{
    public class BitcoinAddress
    {
        public int Id { get; set; }
        
        public string UserId { get; set;}

        public string Address { get; set; }

        public bool IsDefault { get; set; }

        public DateTime WhenCreated { get; set; }
    }
}