namespace isonexwebsite.Data.Entities
{
    public class TeamMember
    {
        public int Id { get; set; }
        public int Order { get; set; }
        public string ImgSrc { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string LinkedInUrl { get; set; }
        public string IconType { get; set; }
        public bool Visible { get; set; }
    }
}