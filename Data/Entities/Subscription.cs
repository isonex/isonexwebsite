using System;

namespace isonexwebsite.Data.Entities
{
    public class Subscription
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public DateTime Timestamp { get; set; }
 
        public string RemoteUserIp { get; set; }

        public string RequestJson { get; set; }

        public string CountryName { get; set; }
        public string CountryCode { get; set; }

        public string MailTrainId { get; set; }
        
        public override string ToString() {
            return Name + "/" + Email;
        }
    }
}