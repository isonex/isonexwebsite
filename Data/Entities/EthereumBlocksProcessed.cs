﻿using System;

namespace IdentityDemo.Data.Entities
{
    public class EthereumBlocksProcessed
    {
        public int Id { get; set; }

        public int BlockNumber { get; set; }
        public int NumberOfTransactionsProcessed { get; set; }
        public DateTime DateProcessed { get; set; }
    }
}
