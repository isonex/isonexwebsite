using System;

namespace isonexwebsite.Data.Entities
{
    public class GlobalConfig
    {
        public int Id { get; set; }
        
        public DateTime TokenSaleStart { get; set;}

    }
}