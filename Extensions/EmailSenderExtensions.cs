using System;
using isonexwebsite.Services;

namespace IdentityDemo.Services
{
    public static class EmailSenderExtensions
    {
        public static void SendEmailConfirmation(this IEmailService emailService, string email, string link)
        {
            emailService.SendEmail(email, "Confirm your email",
                $"Please confirm your account by clicking this link: <a href='{link}'>link</a>", true);
        }
    }
}
