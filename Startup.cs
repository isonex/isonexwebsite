﻿using IdentityDemo.Data;
using IdentityDemo.Models;
using IdentityDemo.Services;
using isonexwebsite.Helpers;
using isonexwebsite.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using System;
using WebApi.Services;
using Microsoft.Extensions.Logging;
using isonexwebsite.Services.MailChimp;
using Microsoft.AspNetCore.DataProtection;
using isonexwebsite.Utils;
using System.Security.Cryptography.X509Certificates;
using IdentityDemo.DbLogging;

namespace IdentityDemo
{
    public class Startup
    {
        public static byte[] array = new byte[32] {187, 178, 195, 97, 204, 67, 215, 152, 110, 161, 78, 17, 155, 234, 217,
        55, 129, 1, 44, 219, 193, 102, 101, 8, 29, 11, 234, 99, 19, 45, 213, 222 };

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<AppSettings>();

            services.Configure<SesEmailSettings>(
                options => Configuration.GetSection("SesEmailSettings").Bind(options)
            );
            
            services.Configure<MailTrainSettings>(
                options => Configuration.GetSection("MailTrainSettings").Bind(options)
            );
            
            services.AddScoped<IContactService, ContactService>();

            services.AddScoped<IEmailService, SesEmailService>();

            services.AddScoped<IRequestSerializer, RequestSerializer>();
            services.AddScoped<IGeoIpService, GeoIpService>();
            services.AddScoped<IGlobalConfigService, GlobalConfigService>();
            services.AddScoped<ISubscriptionsService, SubscriptionsService>();
            services.AddScoped<IMailTrainService, MailTrainService>();
            services.AddScoped<IPriceUdateRepository, PriceUdateRepository>();

            services.AddScoped<IIdentityVerification, IdentityVerification>();
            services.AddScoped<IEthereumPriceHistory, EthereumPriceHistory>();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
                options.Lockout.MaxFailedAccessAttempts = 15;
                options.Lockout.AllowedForNewUsers = true;

                options.SignIn.RequireConfirmedEmail = true;

                // User settings
                options.User.RequireUniqueEmail = true;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.Cookie.Expiration = TimeSpan.FromDays(150);
                // If the LoginPath isn't set, ASP.NET Core defaults 
                // the path to /Account/Login.
                options.LoginPath = "/Account/Login";
                // If the AccessDeniedPath isn't set, ASP.NET Core defaults 
                // the path to /Account/AccessDenied.
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(15);
            });


            byte[] certfordataprot = Convert.FromBase64String(certfordataprotB64);

            services.AddDataProtection()
                 .PersistKeysToFileSystem(new System.IO.DirectoryInfo(@"keys")
                 );
                //  .ProtectKeysWithCertificate(
                //         new X509Certificate2(certfordataprot, Crypt.DecryptString(@"Uuv3cc3G0I/D0hN6KbQAMg==", Startup.array))
                //      );

            services.AddMvc();

            services.AddAutoMapper();

            services.AddResponseCompression();

            services.AddScoped<IBlockchainService, BlockchainService>();
            services.AddSingleton<IDbLogger>(new DbLogger(Configuration.GetConnectionString("DefaultConnection")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddFile("logs/isonex-{Hour}.txt");  // https://nblumhardt.com/2016/10/aspnet-core-file-logger/
            
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseResponseCompression();

            app.UseMvc(
                routes =>
                {
                    // routes.MapRoute(
                    // name: "robots.txt",
                    // template: "robots.txt",
                    // defaults: new { controller = "Home", action = "Robots" }
                    // );

                    routes.MapRoute(
                        name: "contact",
                        template: "Contact/{action}"
                    );

                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
                }
            );
        }


        private static string certfordataprotB64 = @"MIIQMgIBAzCCD+4GCSqGSIb3DQEHAaCCD98Egg/bMIIP1zCCCogGCSqGSIb3DQEHAaCCCnkEggp1MIIKcTCCCm0GCyqGSIb3DQEMCgECoIIJfjCCCXowHAYKKoZIhvcNAQwBAzAOBAgJ94QGcmcLYgICB9AEgglY2iEp7dPkzI3+0snNqTSrmz9LBDR7clnR40yzWVhcd0n4Zy4c4RXw2yVrcRY3wm58UHN3Tpy7h9babLTJnzdvVZd7CpPBqmlzN7ILHbl6ZOM3+DKp2jUq63ZJ9B8TF2Zvdo9owl72le7/ldVmNdZuWDZ0eWU6F/8Q1MIAyedz45IJDFKueXUvl7cdPTWQJAFLKAvKtj+HK1Xnki3CjyA0cRXvYUwBD85ZrS0Es2ZSkWJQXeEPAj2AyCxYC4gvXVhpEsDVKpyhELWe2VVXwVaQLlIAK3UPWU+eAebijqMm4IOcjY+/GC5+NPkkjiC/3vgwl2rjrh292hrw4kP7R39VkD5nyuWiNaFxue3ADNFfiLu6jqLBKm1mG0Fnsx4QYwARKuJ4w0xI22LSD3sxJNQXVa+NhAE/0a/0G815dVAz+yjRU6kSm125WwCNjH3PH6qxPIBDwtgOIF56JVgy2dGJjsgaOilAt3ukkXZRNQ57dJbWmNypJlAo0TgXzd55hGdFVBGgToKGKecjoDOuAaNkNPKukOb5gSk2/nhxXFI31IkejwtfWQbylvvvIx6HHFW2vQpoiie0cuW+KChCNqgdxEXVGPOHv2lGMeh/uQGnWkc2KEi8Sa8MbbcATD0O+DngRnJ7HdTXFkB37kDTNWdlU3T4q+BZ0g2zDnecxH1i/TKlpLqqtqyg/+JZySN8x9P3UieYpw6ygqPC4bt0sJ7dgO/8/xigx/NGt/jRAu0ZfyahgATj0tw726vaQObjkh60yub12F8czN810+7h4odLWzr2Y31ODdQowjRN9otjzuK4ghRyZFHUy2+OFNh0eJL/aLSOmdpeX7kKEwNuZWY8Y0s955UfyPZQZ2qate/InsztVaNpncDYV22jHc77SFSMA5dbleM/odaxwAZbk3slYLLJJol/BI5AOdd8zLml25W/4MPObJDCkc29fT9lNuZasTpA2WaYOAg9M+xV4LEnFcIiFtW13Wz1PzWvRtMXtU5Y7wKzWbcYIDD1WDmB//1kR6HbgPwuVzdEGuVT4jveBd3dq/dKHwWC3VJPlIP1escz/A7bRECcQMEDtYgJ3dIqsL1UN2DpfRSPyDKDjK5+rEd8zXwhNdAQUImmZ8zRqnJt8vQFnrNBHzcxGzY1wHl2n4FtmvnXRbYdh7vL7sJAxR5WVVXoa8qh2gNTcBvvlrOBbad6WjqWYMIaxbIVck2ZbjLx9IfnZAb9K63SO/Lbzhr1XEMn8CGO6345ujfG00EthEbWcyEiW+UgLgxVv7BjwBxKOY0fbChTM3R1YloJmFFVlUh+44jn0GYddycGWrIoLv0FeV8KhGI1DwD5fhfhXyCKfACmiTXz0c8Nquc0Ta4qkHmpW1Rdlb5VBSUQrxOxRvvc298BBnZ+KGtXa2/KLxI8eLiI1E6G01widuoEXyazLXPRVpvdl0sX7Oeper6fbmwoDszvCJixjdufW1mMBetJjxo9/1p8x/I+U63T7JwSp1wahAXfQQ+cR/4Y3q3MlF1WNloV0T3eWA39jU0BttKdh6oT121CE/A2pFCQlP3sPqvgx6MJREO0l1/C4z3j3Nn6BEZXcGY6TsPGzSFaA+o6y46l9hRZS5JmZPOWCuiW6dzTX51aygJInKp3kD3vg8m01/k3pB47fKo/+0mxRDvjChsKaxZcvG1UQ0u3uoCpWXuVHWzrJ9fu8kpDHF7RPlJ10gmNPKCxA1DkiLFkJCAFMgZSa2mJnU0vO5VmKtol9lToMpnu6x/keJ51zT7tg3VcQzGOi8NNi6mpifnfzT/PqtF5kb4gVOXK72kBYpf80WR4P+YuiC7C/f/gm6ll2S4QautnbxldCSZgMg7dE/zSVfJbecHgVEx+EzqFkz7H3KK0CoNrhdlSDR5pqtCoNIPo9eywsfJrOxGWSjYFfkXbhulpZehYBnjf8yjM9l2hqIidKHFWF7t3RFEr4i9lyS/4kQF/eBHM2OjM3Oj0hBLlEMLnLFq7Aw0YsiHti0AWD94E/Qs2pBxqNhpvzfdsorAWYSQ8/iw3jTe3Ss4SDJVdYakBude8xbxHKLV6SO02gXE/PewgvK0lTkSsntwmRg54iG7ApIj6tSe/ASknxXfgq1rb5r2jOX/QZXdgJ7pE69ZJeudB7RRXnd6ZIm+jLksm5xqiHBYhr67x0j7cDyicAJzOnrk/SyZHyBlkj9V8HnafJ6MJLlDze7XgCGXHA5QzAZzP+qXm1lBmbo0WkDoc098OL6YAlrWKPpqucBO8y3Gz3DbUzEacshjpIeRz8j9WosIUQI6olOEjNgwP4TvHErybWA4PBgFev1zDBs1SMNJBdpSzh9yhrZpQ9XofE4hqiM0xRcIOwRoe683rIBTCJ5p5qBPS5cThMr9RCjnbMR88emlO5p7VsJuUJ8Hv93Eowgy2ii2z9rYODOQizbS8ub8kdr7qzgKb1RpTocX4Xb1fhY56jl3hG0LF2orbyHCgr0QPcnshlGMvg5zr1AnWM4eI12f7sUVs3COvVjnVpJWfVv7rm05tPiAtIoqDW2pBp7VW/J+7JRSBV3W/lbzbx2WEzxRHNppCSx9DEYp6EKSpK8l2LC61Z+4QX9tKUeElBKar2dSzw2UlQAXFvPR9LD4TW+D8Kyyl/dypFtUOVyJLrPJu1kf2lvi5wS96v5IdR0l0Fev0RjeYVashMOYT1cSM8kBpVUDM258SvUMeuQf7sE2xenlqczexKh3Py7+URPXSVJvUcthBvcDefMRWs3ESxaMRdQtCy9YdnzkkAeoCLSqk+7aXxFPJtW5Er2PhrmMw/PCpImhq0N1cvGztNaBoXuZii7buUmQUNuyICahe8bySs2EEWlh82HyC/fCGjq+ifm8SKCj4Pkt2f4d7pTb0wgM4hmhsRZQgrQzriEHlb17oJY7GUb33H55MyotiFkuqOow3hoMDmxyxNpDz8Y8Y+tuocAKhOkU9a8bxFeabJC7Vcbf/aNClrP9eCOTgUDX5K37YqON6bk9AgZHhch5PEeiIbD+jlgTxBj/oC0OF1hCGprM6Qskb7yiCfA5GrhHBZUPMSVXeEIJ0Ay9OZ9pKTis+Qi+Otyd2eESseuCtWdD0lDv0GtoF19DYRqdIg/SNwBpdsNTVoMel9GSnGMywguR/oqk154qXlejeLuW5ozho3S4Pyex8KUvOHubHYps5NjGB2zATBgkqhkiG9w0BCRUxBgQEAQAAADBXBgkqhkiG9w0BCRQxSh5IADgAYwAxADQANgAzADYAMQAtADkAMAA4AGUALQA0AGEAOQBhAC0AYQAzADcAOQAtAGMAOQBhADcAMQA1ADgANwA4ADgAOAAzMGsGCSsGAQQBgjcRATFeHlwATQBpAGMAcgBvAHMAbwBmAHQAIABFAG4AaABhAG4AYwBlAGQAIABDAHIAeQBwAHQAbwBnAHIAYQBwAGgAaQBjACAAUAByAG8AdgBpAGQAZQByACAAdgAxAC4AMDCCBUcGCSqGSIb3DQEHBqCCBTgwggU0AgEAMIIFLQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQMwDgQIxWwMira5WxcCAgfQgIIFANYcG8Odd+hTxG65udpq0C05wsq8Dw3jvNv8v8bueHi8OepnrVcyHx0S/1lXGwMaaBaC4BmOAS2ytbNuqDigu279t4IsQ+eL0kIXLfjwyhhaV+t65x6suPYDPLPkMab7M6oN5Q661JwrxiXOywTVDbr/2rqr291UqmtbK6RQXpAlFYiO1b/ZLD3mnPhB8fbjFpMEOJdKQ6F6jgDAeXlRF9LywYGdFdvguAHXm46AU2fYWsEnYKOVeOKNCOpyEfGiZk76odZKpxdYny8Wif4YziweatFDLplzudpERlbo3MuWrSS9V1qlI0hjCCNy9lFYEZPr79odXwE+RnmSbpLWxUzV4oUmfdcmvzZFFvduBidQ52nvrp/kcXSHKgdwa34xs6TEnL5PW/XFtYamTXOJNsCr9dsQZ4wM4HefRtGWCvKmFLoWYIicAW3DreH2KtU/FeXiSf91raXdOGa/UFmcejy5zkb5ynKzpePoDm1l/ey18uzTUTLRB/Ckmfu/QRqimPfn0bUtRkMw0YO4z1p17efLMVRy7k/scO33FHKz4CjVMhcHkezhTuNtaSohtGgdSeqqse6LdTs23E3MQz9E2db02OULxo61hjdswofVNHCFlNJEKPkFR8/ObKwTbXHGh2gQ+gi/UTOMPeE05II67a4PcOKIABGPs9R1M7TzorFhktN/kxhPLQCJWHjhKW+OQzjxfmCLQ3BofVmBDOff0m0put9Wp1EhNGjHM0Z3uxmIKLqxmAakdP2hZqnWxrtaoFeanKp1YSXbiHWtkRgSfh0GEVycpXPdCskyFdJzJuk2AS4YH7woeIRqmowzGmQwBntJYqe2TtdYLFkxnxXoFI+1+mA6GF4RETz1u6JRCzFVWAZjRzlmDiN2TLRZLO6D9Lh53vv8I8NahYr34kvzd5K0wVGRfJUO2soqX80iORG1592+7XLuyFi1gP+MqlhOxkwTxQ4BbQiNjIF4G6fg4X0NUMDkuDMHZrxN/+qF1VcqWYIRW6JjVwNniepYRksA3mifGNmma6p0nO4SkHd7kKYr2cm0vdPje8CpHi24hWlt9zyiBJhJ5ByNLaudkP5mv5J6dIeUxh7NlEkO41m4Sa79wfotRSPZXNABFzCozw6a0Evxt1qoWtR7LctPCXT9cUSVThp0mXDB9VrJ5QKrI0M5DKinDt1ZnfRhrU766VAHOVaLBSb1zuoSwk7rv2dE4ZoXP7C67jKLATTMp+5aQa2gl4WfjiEmyNKEds0OuDAtcsbOmT2oJIMKlLc53YbPtOsxwIR2AjjoTeWaGjgALoWzReW9cDC+7IZWcDIZrnIrexk36nTEwjyMELwzGXrCb1ywF9Y1Rfyo4btC/eAuHe0UaFj8MAitT1lcHfUD7Aq4Cl4MP5KawzSD9KcdPt6ym5XQdagdSiIjP/9t+8WlYQoAPbNtJwwJwGSSuHY1F8I8XhwY3zzFTtz2nL0QLRpMajjxRrkeasyDcf9iO/FtVzNCqNtwuBLXHXhcNMy3CFdDDTwBNhNtn0MfwhBojZ3DMuuH2ZNPaSrQRg7xyEuj9tDThP3kEPd6WZbJtmhMGVWqGLBy1yHHapz7Ra4cdd9YHctMIxr5n3y0cfNAYtDZYbmY2+9L+XamwkAs2smXjH75sS8OsejzXHN7kNJrWaX8A0Q+lHdheJJtJJnDgvUdfENT3nCxsc/EFmocBPg1Xr4jMDswHzAHBgUrDgMCGgQUMXtpIU082DhK+RNNh6doIUiFULcEFNjLv+EWO2eA+PUryYMVRvpaP20UAgIH0A==";
    }
}
