﻿using System.Collections.Generic;
using IdentityDemo;
using isonexwebsite.Utils;

namespace isonexwebsite.Helpers
{
    public class MailTrainSettings  
    {
        private string pass;
        private bool decrypted = false;
        private static object padlock = new object();

        public string ApiUrl { get; set;}
        public string AccessToken { 
            get {
                lock(padlock) {
                    if (decrypted) {
                        return pass;
                    }

                    if (!string.IsNullOrEmpty(AccessTokenEnc)) {
                        pass = Crypt.DecryptString(AccessTokenEnc, Startup.array);
                        decrypted = true;
                    }
                     return pass;
                }
            }
            set {
                lock(padlock) {
                    this.pass = value;
                    decrypted = true;
                }
            }
        }
        public string AccessTokenEnc { get; set; }

        public string AccountListId { get; set; }
        public string NewsletterListId { get; set; }
    }
}