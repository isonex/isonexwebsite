using System.Collections.Generic;
using IdentityDemo;
using isonexwebsite.Utils;

namespace isonexwebsite.Helpers
{
    public class SesEmailSettings  
    {
        private string decryptedAccessKeyId;
        private string decryptedSecretKey;
        
        private bool isDecryptedAccessKeyId = false;
        private bool isDecryptedSecretKey = false;

        private static object padlock = new object();

        public string AccessKeyId
        {
            get
            {
                lock (padlock)
                {
                    if (isDecryptedAccessKeyId) return decryptedAccessKeyId;

                    if (!string.IsNullOrEmpty(AccessKeyIdEnc))
                    {
                        decryptedAccessKeyId = Crypt.DecryptString(AccessKeyIdEnc, Startup.array);
                        isDecryptedAccessKeyId = true;
                    }
                    return decryptedAccessKeyId;
                }
            }
        }

        public string SecretKey
        {
            get
            {
                lock (padlock)
                {
                    if (isDecryptedSecretKey) return decryptedSecretKey;
                    
                    if (!string.IsNullOrEmpty(SecretKeyEnc))
                    {
                        decryptedSecretKey = Crypt.DecryptString(SecretKeyEnc, Startup.array);
                        isDecryptedSecretKey = true;
                    }

                    return decryptedSecretKey;
                }
            }
        }

        public string AccessKeyIdEnc { get; set; }
        public string SecretKeyEnc { get; set; }

        public string FromEmailAddress { get; set; }
        public List<string> OurEmailAddresses { get; set; }
    }
}