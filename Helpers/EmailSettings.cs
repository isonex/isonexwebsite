using System.Collections.Generic;
using IdentityDemo;
using isonexwebsite.Utils;

namespace isonexwebsite.Helpers
{
    public class EmailSettings  
    {
        private string pass;
        private bool decrypted = false;
        private static object padlock = new object();

        public string HostName { get; set; }
        public string Username { get; set; }
        public string Password { 
            get {
                lock(padlock) {
                    if (decrypted) {
                        return pass;
                    }

                    if (!string.IsNullOrEmpty(PwdEnc)) {
                        pass = Crypt.DecryptString(PwdEnc, Startup.array);
                        decrypted = true;
                    }
                     return pass;
                }
            }
            set {
                lock(padlock) {
                    this.pass = value;
                    decrypted = true;
                }
            }
        }
        public string PwdEnc { get; set; }
        public int Port { get; set; }

        public string FromEmailAddress { get; set; }
        public List<string> OurEmailAddresses { get; set; }
    }
}