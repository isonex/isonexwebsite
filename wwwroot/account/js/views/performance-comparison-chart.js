$(function () {
	'use strict';

	//convert Hex to RGBA
	function convertHex(hex, opacity) {
		hex = hex.replace('#', '');
		var r = parseInt(hex.substring(0, 2), 16);
		var g = parseInt(hex.substring(2, 4), 16);
		var b = parseInt(hex.substring(4, 6), 16);

		var result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
		return result;
	}

	//Random Numbers
	function random(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}



	//Main Chart
	var elements = 27;
	var data1 = [];
	var data2 = [];
	var data4 = [];

	for (var i = 0; i <= elements; i++) {
		data1.push(random(14000, 16000));
		data2.push(random(14000, 16000));
		data4.push(random(14000, 16000));
	}

	var data = {
		labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S'],
		datasets: [
			{
				label: '',
				backgroundColor: convertHex($.brandInfo, 10),
				borderColor: $.brandSuccess,
				pointHoverBackgroundColor: '#fff',
				borderWidth: 2,
				data: data1
			},
			{
				label: '',
				backgroundColor: 'transparent',
				borderColor: $.brandInfo,
				pointHoverBackgroundColor: '#fff',
				borderWidth: 2,
				data: data2
			},
			{
				label: '',
				backgroundColor: 'transparent',
				borderColor: $.brandDanger,
				pointHoverBackgroundColor: '#fff',
				borderWidth: 2,
				borderDash: [8, 5],
				data: window.crixPrices
			},
			{
				label: '',
				backgroundColor: 'transparent',
				borderColor: $.brandWarning,
				pointHoverBackgroundColor: '#fff',
				borderWidth: 2,
				data: data4
			}
		]
	};

	var options = {
		maintainAspectRatio: false,
		legend: {
			display: false
		},
		scales: {
			xAxes: [{
				gridLines: {
					drawOnChartArea: false,
				}
			}],
			yAxes: [{
				ticks: {
					beginAtZero: true,
					maxTicksLimit: 5,
					stepSize: Math.ceil(2000 / 5),
					max: 12000,
					min: 18000
				}
			}]
		},
		elements: {
			point: {
				radius: 0,
				hitRadius: 10,
				hoverRadius: 4,
				hoverBorderWidth: 3,
			}
		},
	};
	var ctx = $('#performance-comparison-chart');
	var mainChart = new Chart(ctx, {
		type: 'line',
		data: data,
		options: options
	});


});
