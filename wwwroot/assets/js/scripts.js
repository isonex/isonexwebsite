(function(window, undefined) {
  'use strict';

  /*
  NOTE:
  ------
  PLACE HERE YOUR OWN JAVASCRIPT CODE IF NEEDED
  WE WILL RELEASE FUTURE UPDATES SO IN ORDER TO NOT OVERWRITE YOUR JAVASCRIPT CODE PLEASE CONSIDER WRITING YOUR SCRIPT HERE.  */


  window.showEtherPaymentMethod = function() {

	$('#payment-method').hide();
	$('#ether-payment').show();
    $('#bitcoin-payment').hide();
  }

  window.showBitcoinPaymentMethod = function() {
	$('#payment-method').hide();
    $('#ether-payment').hide();
    $('#bitcoin-payment').show();
  }


	$('#btnUpdatePersoanlEthereumAddress').click(function () {
		var newAddress = $('#newPersoanlEthereumAddress').val();
		updatePersonalAddress(newAddress);
	});

	window.updatePersonalAddress = function (newAddress) {
		$.post('/Account/UpdatePersonalAddress?newAddress=' + newAddress);
	}



	//slide tiles in
	//https://codepen.io/simoncodrington/pen/Mwgqqd

	var animation_elements = $.find('.animation-element');
	var web_window = $(window);

	function check_if_in_view() {
		var window_height = web_window.height();
		var window_top_position = web_window.scrollTop();
		var window_bottom_position = (window_top_position + window_height);

		$.each(animation_elements, function () {

			//get the element sinformation
			var element = $(this);
			var element_height = $(element).outerHeight();
			var element_top_position = $(element).offset().top;
			var element_bottom_position = (element_top_position + element_height);

			//check to see if this current container is visible (its viewable if it exists between the viewable space of the viewport)
			if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
				element.addClass('in-view');
			} else {
				element.removeClass('in-view');
			}
		});
	}

	$(window).on('scroll resize', function () {
		check_if_in_view()
	})

	$(window).trigger('scroll');



})(window);