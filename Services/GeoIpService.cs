using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using isonexwebsite.Helpers;
using isonexwebsite.Services;
using MaxMind.GeoIP2;
using MaxMind.GeoIP2.Exceptions;
using MaxMind.GeoIP2.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using MaxMind.GeoIP2.Model;

namespace WebApi.Services
{
    public class GeoIpService : IGeoIpService
    {
        private readonly IEmailService _emailService;
        private readonly DatabaseReader _reader;
        private readonly ILogger<GeoIpService> _logger;

        public GeoIpService(IOptions<AppSettings> appSettings, IEmailService emailService, ILogger<GeoIpService> logger) {
            _emailService = emailService;
            _logger = logger;

            _logger.LogInformation("GeoIpService constructor - about to create DatabaseReader(" + appSettings.Value.GeoIpFile + ")");

            try {
                _reader = new DatabaseReader(appSettings.Value.GeoIpFile);
            }
            catch(Exception ex) {
                var guid = Guid.NewGuid();
                string message = string.Format("[{0}] Could not create DatabaseReader: ", guid);
                _logger.LogError(message + ex);
                _emailService.SendAlert(message, ex);
            }
        }

        public IsonexCountryResponse GetCountryFromIp(string ipAddressString)
        {
            try {
                if (_reader != null) {
                    IPAddress ip = IPAddress.Parse(ipAddressString);
                    var result = _reader.Country(ip);
                    var isonexCountryResponse = new IsonexCountryResponse { Name = result.Country.Name, IsoCode = result.Country.IsoCode};
                    return isonexCountryResponse;
                }
                else {
                    var isonexCountryResponse = new IsonexCountryResponse { Name = "!!!maxmind error - reader is null!!!", IsoCode = null };
                    return isonexCountryResponse;
                }
            }
            catch(AddressNotFoundException) {
                return null;
            }
        }
    }

}