using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using isonexwebsite.Data.Entities;

namespace isonexwebsite.Services
{
    public interface IEmailService
    {
        Task SendNotification(Contact contact);
        Task SendAlert(string message, Exception exc);
        Task SendEmail(List<string> recipientEmailAddresses, string subject, string message, bool asHtml);
        Task SendEmail(string recipientEmailAddress, string subject, string message, bool asHtml);

    }
}