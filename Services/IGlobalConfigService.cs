using System.Collections.Generic;
using isonexwebsite.Data.Entities;

namespace isonexwebsite.Services
{
    public interface IGlobalConfigService
    {
        GlobalConfig GetGlobalConfig();
        List<TeamMember> GetTeamMembers(bool visibleOnly);
    }
}