﻿using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using IdentityDemo.Models.AccountViewModels;
using Newtonsoft.Json.Linq;

namespace IdentityDemo.Services
{
    public class IdentityVerification : IIdentityVerification
    {
        private const string baseUrl = "https://shuftipro.com/api/";
        private const string clientId = "af8292aee12af6a563f3ff073be848ac5eceb46b55f3761d857ec8de8672c683";
        private const string sekretKey = "rbaRSADpEflAvEBjSv8jKqAPec1hXgUD";
        private static readonly HttpClient _client = new HttpClient();

        public async Task<bool> VerifyAsync(string userEmail, VerifyViewModel verifyViewModel)
        {
            var byteArray = Encoding.ASCII.GetBytes($"{clientId}:{sekretKey}");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, baseUrl);
            request.Content = new StringContent(GetRequestBody(userEmail, verifyViewModel), Encoding.UTF8, "application/json");

            await _client.SendAsync(request).ContinueWith(responseTask =>
            {
                Console.WriteLine("Response: {0}", responseTask.Result);
            });

            return true;
        }

        private string GetRequestBody(string userEmail, VerifyViewModel verifyViewModel)
        {
            // Why do we need to pass in the email?
            // What about verifying id card number on it's own, no document
            //var address = "180 The Willows Residence Flat 3, Triq Lorenzo Manche, Attard, Malta";
            var documentTypeForAddressCheck = "id_card"; //  "passport", "id_card",  "driving_license", "credit_or_debit_card"
            //var request = GetRequest("Fikre", null, "Leguesse", "MT", "fikreleguesse@hotmail.com", new DateTime(1986, 06, 29), address, documentTypeForAddressCheck);

            var address = $"{verifyViewModel.AddressLine1} {verifyViewModel.AddressLine2} {verifyViewModel.PostalCode} {verifyViewModel.CountryName}";

            var request = GetRequest(verifyViewModel.FirstName, verifyViewModel.MiddleName, verifyViewModel.LastName, verifyViewModel.CountryCode, userEmail, new DateTime(verifyViewModel.DobYear, verifyViewModel.DobMonth, verifyViewModel.DobDay), address, documentTypeForAddressCheck);

            return request;
        }

        private string GetRequest(string firstName, string middleName, string lastName, string countryCode, string email, DateTime dob, string fullAddress, string documentTypeForAddressCheck)
        {
            var request = new
            {
                reference = Guid.NewGuid().ToString().Replace("-", "").ToLower(),
                country = countryCode,
                email,
                background_checks = new
                {
                    name = new
                    {
                        first_name = firstName,
                        last_name = lastName,
                        middle_name = middleName
                    },
                    dob = dob.ToString("yyyy-MM-dd")
                },
                callback_url = "http://leguesse.com",
               // address = new {
               //     proof = "",
               //     full_address = fullAddress,
               //     name = new {
               //         first_name = firstName, 			            //last_name = lastName, 			            //middle_name = middleName, 			            //fuzzy_match = "1"
               //     },
               //     supported_types = new[] {
               //         documentTypeForAddressCheck
               //     }
               // }
            };

            var result = JsonConvert.SerializeObject(request);

            return result;
        }

        public async Task<VerificationStatus> CheckVerificationStatusAsync(string reference)
        {
            var byteArray = Encoding.ASCII.GetBytes($"{clientId}:{sekretKey}");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"{baseUrl}/status");
            request.Content = new StringContent(JsonConvert.SerializeObject(new { reference }), Encoding.UTF8, "application/json");

            var result = await _client.SendAsync(request);

            var res = await result.Content.ReadAsStringAsync();
            dynamic dynaimcContent = JObject.Parse(res);
            if (dynaimcContent.error != null)
            {
                return VerificationStatus.Error;
            }
            else if (dynaimcContent.@event == "verification.error??")
            {
                return VerificationStatus.Error;
            }
            else if (dynaimcContent.@event == "verification.accepted")
            {
                return VerificationStatus.IsVerified;
            }
            else if (dynaimcContent.@event == "verification.rejected")
            {
                return VerificationStatus.FailedVerification;
            }
            else
            {
                return VerificationStatus.UnknownResponse;
            }
        }
    }
}























































































































































































































































