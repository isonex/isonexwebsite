using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using static isonexwebsite.Services.MailChimp.AddMemberToListRequest;

namespace isonexwebsite.Services.MailChimp
{
    public class AddMemberToListResponse
    {
        [JsonProperty("id")]
        public string MailChimpId { get; set; }

        [JsonProperty("unique_email_id")]
        public string MailChimpUniqueEmailId { get; set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]        
        public MemberStatusEnum Status { get; set; }

        [JsonIgnore]
        public bool AlreadyExists { get; set; }

        [JsonIgnore]
        public bool Error { get; set; }

        [JsonIgnore]
        public AddMemberToListErrorResponse ErrorResponse { get; set; }

    }
}