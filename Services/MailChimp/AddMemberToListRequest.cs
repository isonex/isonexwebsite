using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace isonexwebsite.Services.MailChimp
{
    public class AddMemberToListRequest
    {
        [JsonProperty("email_address")]
        public string EmailAddress { get; set; }

        [JsonProperty("email_type")]
        [JsonConverter(typeof(StringEnumConverter))]                
        public EmailTypeEnum EmailType { get; set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]        
        public MemberStatusEnum Status { get; set; }

        [JsonProperty("ip_signup")]
        public string IpSignup { get; set; }

        public enum EmailTypeEnum
        {
            html, text
        }

        public enum MemberStatusEnum
        {
            subscribed, unsubscribed, cleaned, pending
        }
    }
}