namespace isonexwebsite.Services.MailChimp
{
    public interface IMailTrainService {
        string AddSubscriberToNewsletterList(MailTrainAddMemberToListRequest request);
        
        string AddAccountToList(MailTrainAddMemberToListRequest request);
    }
}