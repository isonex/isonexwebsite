using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace isonexwebsite.Services.MailChimp
{
    public class AddMemberToListErrorResponse
    {
        [JsonProperty("type")]
        public string ProblemType { get; set; }

        [JsonProperty("title")]
        public string ErrorTitle { get; set; }

        [JsonProperty("status")]
        [JsonConverter(typeof(StringEnumConverter))]        
        public HttpStatusCode HttpStatusCode { get; set; }

        [JsonProperty("detail")]
        public string ErrorMessage { get; set; }

        [JsonProperty("instance")]
        public string InstanceID { get; set; }

        public override string ToString() {
            var s = new StringBuilder();

            s.Append("ProblemType: ").AppendLine(this.ProblemType);
            s.Append("ErrorTitle: ").AppendLine(this.ErrorTitle);
            s.Append("HttpStatusCode: ").AppendLine(this.HttpStatusCode.ToString());
            s.Append("ErrorMessage: ").AppendLine(this.ErrorMessage);
            s.Append("InstanceID: ").AppendLine(this.InstanceID);

            return s.ToString();
        }

    }
}