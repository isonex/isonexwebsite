﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace isonexwebsite.Services.MailChimp
{
    public class MailTrainAddMemberToListRequest
    {
        [JsonProperty("EMAIL")]
        public string EmailAddress { get; set; }

        [JsonProperty("FIRST_NAME")]
        public string FirstName { get; set; }

        [JsonProperty("LAST_NAME")]
        public string LastName { get; set; }

    }
}