﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using static isonexwebsite.Services.MailChimp.AddMemberToListRequest;

namespace isonexwebsite.Services.MailChimp
{
    public class MailTrainAddMemberToListResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("data")]
        public Dictionary<string, string> Data { get; set; }
    }
}