﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using isonexwebsite.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace isonexwebsite.Services.MailChimp
{
    public class MailTrainService : IMailTrainService 
    {
        private readonly MailTrainSettings settings;
        private readonly ILogger<MailTrainService> logger;

        private readonly string apiAuth;

        public MailTrainService(IOptions<MailTrainSettings> settings, ILogger<MailTrainService> logger)
        {
            this.settings = settings.Value;

            this.logger = logger;
        }

        public string AddSubscriberToNewsletterList(MailTrainAddMemberToListRequest request)
        {
            return AddMemberToList(request, settings.NewsletterListId);
        }

        public string AddAccountToList(MailTrainAddMemberToListRequest request)
        {
            return AddMemberToList(request, settings.AccountListId);
        }

        private string AddMemberToList(MailTrainAddMemberToListRequest request, string listId) {
            var json = JsonConvert.SerializeObject(request);

            var url = settings.ApiUrl + "/subscribe/" + listId + "?access_token=" + settings.AccessToken;
            var req = HttpWebRequest.Create(url);
            req.ContentType = "application/json";
            req.Method = "POST";
            
            using (var streamWriter = new StreamWriter(req.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)req.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var s = streamReader.ReadToEnd();
                MailTrainAddMemberToListResponse response;
                response = JsonConvert.DeserializeObject<MailTrainAddMemberToListResponse>(s);

                if (string.IsNullOrEmpty(response.Error)) {
                    var id = response.Data["id"];
                    return id;
                }
                else {
                    throw new Exception(string.Format("An error occurred while attempting to add [{0}/{1}] to subscription list [{2}]: ", response.Error));
                }
            }

        }

        private HttpClient GetClient() {
            var http = new HttpClient();
            return http;
        }
    }
}