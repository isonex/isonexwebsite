﻿using isonexwebsite.Data.Entities;
using System.Collections.Generic;
using IdentityDemo.Data;
using System.Linq;

namespace IdentityDemo.Services
{
    public interface ISubscriptionsService
    {
        void Subscribe(Subscription subscription);
    }

    public class SubscriptionsService : ISubscriptionsService
    {
        private ApplicationDbContext _context;

        public SubscriptionsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Subscribe(Subscription subscription)
        {
            var existingSubscription = _context.Subscriptions.FirstOrDefault(e => e.Email == subscription.Email);

            if (existingSubscription == null)
            {
                _context.Subscriptions.Add(subscription);
            }

            _context.SaveChanges();
        }
    }
}
