using System.Collections.Generic;
using isonexwebsite.Data.Entities;

namespace isonexwebsite.Services
{
    public interface IContactService
    {
        void StoreContact(Contact contact);
        List<Contact> GetAll();
    }
}