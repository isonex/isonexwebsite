using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using IdentityDemo.DbLogging;
using IdentityDemo.Utils;
using isonexwebsite.Data.Entities;
using isonexwebsite.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace isonexwebsite.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;
        private readonly ILogger<EmailService> _logger;
        private readonly IDbLogger dbLogger;

        public EmailService(IOptions<EmailSettings> emailSettings, ILogger<EmailService> logger, IDbLogger dbLogger)
        {
            _emailSettings = emailSettings.Value;
            _logger = logger;
            this.dbLogger = dbLogger;
        }


        public async Task SendEmail(string email, string subject, string message, bool asHtml)
        {
            await SendEmail(new List<string>() { email }, subject, message, asHtml);
        }
        public async Task SendEmail(List<string> recipientEmailAddresses, string subject, string message, bool asHtml)
        {
            var msg = $"about to send email to {recipientEmailAddresses.ToCommaSeparatedString()}: {subject}";
            _logger.LogInformation(msg);
            var correlationId = await dbLogger.LogWithNewCorrelationId(msg, "email");
            
            try {
                using (SmtpClient client = new SmtpClient(_emailSettings.HostName, _emailSettings.Port))
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);

                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(_emailSettings.FromEmailAddress);
                    foreach (var emailAddr in recipientEmailAddresses)
                    {
                        mailMessage.To.Add(new MailAddress(emailAddr));
                    }

                    mailMessage.Body = message;
                    mailMessage.IsBodyHtml = asHtml;
                    mailMessage.Subject = subject;
                    client.Send(mailMessage);

                    _logger.LogInformation("email sent");
                }
            }
            catch(Exception ex) {
                _logger.LogError($"[{correlationId}] EmailService.SendEmail error: {ex}");
                await dbLogger.Log("EmailService.SendEmail to {email} error: " + ex.ToString(), "email", correlationId);
                throw;
            }
        }

        public async Task SendAlert(string message, Exception exc)
        {
            _logger.LogInformation("sending alert: " + message + " - " + exc);
            try {
                using (SmtpClient client = new SmtpClient(_emailSettings.HostName, _emailSettings.Port))
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);

                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(_emailSettings.FromEmailAddress);
                    foreach (var ourEmailAddress in _emailSettings.OurEmailAddresses)
                    {
                        mailMessage.To.Add(ourEmailAddress);
                    }
                    mailMessage.Body = renderAlertBody(message, exc);
                    mailMessage.Subject = "isonex alert: " + message;
                    client.Send(mailMessage);

                    _logger.LogInformation("notification sent");
                }
            }
            catch(Exception ex) {
                _logger.LogError("EmailService.SendAlert error: " + ex.ToString());
                throw;
            }


        }

        public async Task SendNotification(Contact contact)
        {
            _logger.LogInformation("about to send notification: " + contact.ToString());
            try {
                using (SmtpClient client = new SmtpClient(_emailSettings.HostName, _emailSettings.Port))
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);

                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress(_emailSettings.FromEmailAddress);
                    foreach (var ourEmailAddress in _emailSettings.OurEmailAddresses)
                    {
                        mailMessage.To.Add(ourEmailAddress);
                    }
                    mailMessage.Body = renderBody(contact);
                    mailMessage.Subject = "isonex contact " + renderFrom(contact);
                    client.Send(mailMessage);

                    _logger.LogInformation("notification sent");
                }
            }
            catch(Exception ex) {
                _logger.LogError("EmailService.SendNotification error: " + ex.ToString());
                throw;
            }


        }

        private string renderFrom(Contact contact) {
            string from = string.Format("from: {0} [{1}]", contact.Name, contact.Email);
            return from;
        }
        private string renderBody(Contact contact)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("Isonex Contact: ").AppendLine()
                .AppendLine(renderFrom(contact)).AppendLine()
                .AppendLine("---------------------------------------").AppendLine()
                .AppendLine(contact.Message).AppendLine()
                .AppendLine("=================");

            return sb.ToString();
        }

        private string renderAlertBody(string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("isonex.io alert: ").AppendLine()
                .AppendLine("- message --------------------------------------").AppendLine()
                .AppendLine(message).AppendLine()
                .AppendLine("= exception ====================================").AppendLine()
                .AppendLine(ex == null ? "<NULL>" : ex.ToString())
                .AppendLine("---");

            return sb.ToString();
        }
    }
}