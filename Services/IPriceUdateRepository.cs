﻿using IdentityDemo.Data.Entities;
using System;
using System.Collections.Generic;

namespace IdentityDemo.Services
{
    public interface IPriceUdateRepository
    {
        IEnumerable<PriceHistory> GetCrixPriceHistory(DateTime from, DateTime to);
    }
}
