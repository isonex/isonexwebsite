using System;
using System.Collections.Generic;
using System.Linq;
using IdentityDemo.Data;
using isonexwebsite.Data.Entities;
using Microsoft.Extensions.Logging;

namespace isonexwebsite.Services
{
    public class GlobalConfigService : IGlobalConfigService
    {
        private ApplicationDbContext _context;
        private readonly ILogger<GlobalConfigService> _logger;

        public GlobalConfigService(ApplicationDbContext context,
                ILogger<GlobalConfigService> logger
        )
        {
            _context = context;
            _logger = logger;
        }

        public GlobalConfig GetGlobalConfig()
        {
            var config = new GlobalConfig
            {
                Id = 0,
                TokenSaleStart = new DateTime(2019, 01, 01, 12, 0, 0)
            };

            try
            {
                config = _context.GlobalConfigs.OrderByDescending(row => row.Id).FirstOrDefault();

                if (config == null)
                {
                    _logger.LogWarning("Could not find GlobalConfig - returning default");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError("Could not load global config from db, returning default: " + ex);

            }
            return config;
        }

        public List<TeamMember> GetTeamMembers(bool visibleOnly) {
            var teamMembers = _context.TeamMembers
                .Where(tm => visibleOnly ? tm.Visible : true)
                .OrderBy(tm => tm.Order).ToList();
                
            return teamMembers;
        }
    }
}