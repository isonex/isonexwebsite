﻿using IdentityDemo.Data;
using IdentityDemo.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IdentityDemo.Services
{
    public class PriceUdateRepository : IPriceUdateRepository
    {
        private ApplicationDbContext _context;

        public PriceUdateRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<PriceHistory> GetCrixPriceHistory(DateTime from, DateTime to)
        {
            return _context.CrixDailyPriceHistory.Where(c => c.Date >= from && c.Date <= to).ToList();
        }
    }
}
