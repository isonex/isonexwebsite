namespace WebApi.Services
{
    public class IsonexCountryResponse
    {
        public string Name { get; set; }
        public string IsoCode { get; set; }
    }
}