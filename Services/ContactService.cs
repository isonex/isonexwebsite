using System;
using System.Collections.Generic;
using System.Linq;
using IdentityDemo.Data;
using isonexwebsite.Data.Entities;
using isonexwebsite.Services;

namespace WebApi.Services
{
    public class ContactService : IContactService
    {
        private ApplicationDbContext _context;

        public ContactService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Contact> GetAll()
        {
            var contacts = _context.Contacts.OrderByDescending(o => o.Timestamp).ToList();
            return contacts;
        }

        public void StoreContact(Contact contact)
        {
            _context.Contacts.Add(contact);
            _context.SaveChanges();
        }
    }
}