using isonexwebsite.Data.Entities;
using System.Collections.Generic;
using IdentityDemo.Data;
using System.Linq;
using System;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using IdentityDemo.Data.Entities;
using System.Numerics;

public class BlockchainService : IBlockchainService
{
    private ApplicationDbContext _context;

    public BlockchainService(ApplicationDbContext context)
    {
        _context = context;
    }

    public IEnumerable<EthereumAddress> GetUserEthereumAddresses(string userId)
    {
        return _context.EthereumAddress.Where(a => a.UserId == userId);
    }

    public IEnumerable<BitcoinAddress> GetUserBitcoinAddresses(string userId)
    {
        return _context.BitcoinAddress.Where(a => a.UserId == userId);
    }

    public string GetUserPersonalEthereumAddress(string userId)
    {
        return _context.PersonalEthereumAddress.FirstOrDefault(a => a.UserId == userId)?.Address;
    }

    public IEnumerable<PersonalEthereumAddress> GetUserPersonalEthereumAddresses(string userId)
    {
        return _context.PersonalEthereumAddress.Where(a => a.UserId == userId);
    }

    public void SetUserPersonalEthereumAddresses(string userId, string address)
    {
        var personalEthereumAddress = _context.PersonalEthereumAddress.FirstOrDefault(a => a.UserId == userId);

        if (personalEthereumAddress != null)
        {
            personalEthereumAddress.Address = address;
        }
        else
        {
            _context.PersonalEthereumAddress.Add(new PersonalEthereumAddress()
            {
                Address = address,
                UserId = userId
            });
        }

        _context.SaveChanges();
    }

    public IEnumerable<ParticipantEthereumTransactions> GetMyEthereumDepositStatuses(string userId)
    {
        var addresses = _context.EthereumAddress.Where(a => a.UserId == userId);
        var ids = addresses.Select(a => a.Id);

        var txs = _context.ParticipantEthereumTransactions.Where(t => ids.Contains(t.DepositingAddressId)).Include(t => t.DepositingAddress).ToList();

        return txs;
    }

    public IEnumerable<ParticipantBitcoinTransactions> GetMyBitcoinDepositStatuses(string userId)
    {
        var addresses = _context.BitcoinAddress.Where(a => a.UserId == userId);
        var ids = addresses.Select(a => a.Id);

        var txs = _context.ParticipantBitcoinTransactions.Where(t => ids.Contains(t.DepositingAddressId)).Include(t => t.DepositingAddress).ToList();

        return txs;
    }

    public void AssignDepositingAddresses(string userId)
    {
        var first = _context.EthereumAddress.OrderBy(e => e.Id).FirstOrDefault(e => e.UserId == null);

        if (first != null)
        {
            first.UserId = userId;
        }
        else
        {
            _context.EthereumAddress.Add(new EthereumAddress()
            {
                UserId = userId,
                WhenCreated = DateTime.Now
            });
        }

        var firstBitcoin = _context.BitcoinAddress.OrderBy(e => e.Id).FirstOrDefault(e => e.UserId == null);

        if (firstBitcoin != null)
        {
            firstBitcoin.UserId = userId;
        }
        else
        {
            _context.BitcoinAddress.Add(new BitcoinAddress()
            {
                UserId = userId,
                WhenCreated = DateTime.Now
            });
        }

        _context.SaveChanges();
    }
}

public interface IBlockchainService
{
    IEnumerable<EthereumAddress> GetUserEthereumAddresses(string userId);
    IEnumerable<BitcoinAddress> GetUserBitcoinAddresses(string userId);
    string GetUserPersonalEthereumAddress(string userId);
    IEnumerable<PersonalEthereumAddress> GetUserPersonalEthereumAddresses(string userId);
    void AssignDepositingAddresses(string id);
    void SetUserPersonalEthereumAddresses(string userId, string newAddress);
    IEnumerable<ParticipantEthereumTransactions> GetMyEthereumDepositStatuses(string userId);
    IEnumerable<ParticipantBitcoinTransactions> GetMyBitcoinDepositStatuses(string userId);
    
}