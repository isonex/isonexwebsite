﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System;

namespace IdentityDemo.Services
{
    public class EthereumPriceHistory : IEthereumPriceHistory
    {
        public decimal GetCurrentPrice()
        {
            var unixTimestamp = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds();

            var url = "https://min-api.cryptocompare.com/data/histoday?fsym=ETH&tsym=USD&limit=2&toTs=" + unixTimestamp;

            HttpClient _client = new HttpClient();
            var json = _client.GetStringAsync(url).Result;

            dynamic dyn = JsonConvert.DeserializeObject<JObject>(json);

            var res = dyn.Data[2].close;
            return res;
        }
    }
}
