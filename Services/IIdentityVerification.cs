﻿using IdentityDemo.Models.AccountViewModels;
using System.Threading.Tasks;

namespace IdentityDemo.Services
{
    public interface IIdentityVerification
    {
        Task<bool> VerifyAsync(string userEmail, VerifyViewModel verifyViewModel);
        Task<VerificationStatus> CheckVerificationStatusAsync(string reference);
    }

    public enum VerificationStatus
    {
        IsVerified,
        IsPending ,
        FailedVerification,
        Error,
        UnknownResponse
    }
}
