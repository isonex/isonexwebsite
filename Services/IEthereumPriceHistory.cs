﻿
namespace IdentityDemo.Services
{
    public interface IEthereumPriceHistory
    {
        decimal GetCurrentPrice();
    }
}
