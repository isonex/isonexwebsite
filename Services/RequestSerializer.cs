using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace isonexwebsite.Helpers
{
    public interface IRequestSerializer
    {
        string SerializeRequest(HttpRequest request);
    }

    public class RequestSerializer : IRequestSerializer
    {
        public string SerializeRequest(HttpRequest request)
        {
            var serialized = JsonConvert.SerializeObject(request.Headers);
            return serialized;
        }
    }
}