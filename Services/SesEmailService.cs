using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using IdentityDemo.DbLogging;
using IdentityDemo.Utils;
using isonexwebsite.Data.Entities;
using isonexwebsite.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace isonexwebsite.Services
{
    public class SesEmailService : IEmailService
    {
        private readonly SesEmailSettings sesEmailSettings;
        private readonly ILogger<SesEmailService> _logger;
        private readonly IDbLogger dbLogger;

        public SesEmailService(IOptions<SesEmailSettings> sesEmailSettings, ILogger<SesEmailService> logger, IDbLogger dbLogger)
        {
            this.sesEmailSettings = sesEmailSettings.Value;
            _logger = logger;
            this.dbLogger = dbLogger;
        }

        public async Task SendEmail(string receiverEmailAddress, string subject, string message, bool asHtml)
        {
            await SendEmail(new List<string>() { receiverEmailAddress }, subject, message, asHtml);
        }
        public async Task SendEmail(List<string> receiverEmailAddresses, string subject, string message, bool asHtml)
        {
            var receiverAddressesString = receiverEmailAddresses.ToCommaSeparatedString();

            var msg = $"SES about to send email to {receiverAddressesString}: {subject}";
            _logger.LogInformation(msg);
            var correlationId = await dbLogger.LogWithNewCorrelationId(msg, "email");

            using (var client = new AmazonSimpleEmailServiceClient(sesEmailSettings.AccessKeyId, sesEmailSettings.SecretKey, RegionEndpoint.EUWest1))  // https://docs.aws.amazon.com/general/latest/gr/rande.html#ses_region
            {
                Body body;
                if (asHtml)
                {
                    body = new Body
                    {
                        Html = new Content
                        {
                            Charset = "UTF-8",
                            Data = message
                        }
                    };
                }
                else
                {
                    body = new Body
                    {
                        Text = new Content
                        {
                            Charset = "UTF-8",
                            Data = message
                        }
                    };
                }

                var sendRequest = new SendEmailRequest
                {
                    Source = sesEmailSettings.FromEmailAddress,
                    Destination = new Destination
                    {
                        ToAddresses = receiverEmailAddresses
                    },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = body
                    },
                };
                try
                {
                    _logger.LogInformation($"[{correlationId}] Sending email using Amazon SES...");
                    await dbLogger.Log($"Sending email using Amazon SES...", "email", correlationId);
                    var response = client.SendEmailAsync(sendRequest);
                    _logger.LogInformation($"[{correlationId}] Sent successfully.");
                    await dbLogger.Log($"Sent successfully.", "email", correlationId);
                }
                catch (Exception ex)
                {
                    _logger.LogInformation($"[{correlationId}] Error: {ex}");
                    await dbLogger.Log($"Error: {ex}", "email", correlationId);
                    throw;
                }
            }
        }


        public async Task SendAlert(string message, Exception exc)
        {
            await SendEmail(sesEmailSettings.OurEmailAddresses, $"isonex alert: {message}", $"isonex alert: <br>{message}<br><br>{exc}", true);

        }

        public async Task SendNotification(Contact contact)
        {
            await SendEmail(sesEmailSettings.OurEmailAddresses, $"isonex contact {renderFrom(contact)}", renderBody(contact), false);
        }

        private string renderFrom(Contact contact) {
            string from = string.Format("from: {0} [{1}]", contact.Name, contact.Email);
            return from;
        }
        private string renderBody(Contact contact)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("Isonex Contact: ").AppendLine()
                .AppendLine(renderFrom(contact)).AppendLine()
                .AppendLine("---------------------------------------").AppendLine()
                .AppendLine(contact.Message).AppendLine()
                .AppendLine("=================");

            return sb.ToString();
        }

        private string renderAlertBody(string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            
            sb.AppendLine("isonex.io alert: ").AppendLine()
                .AppendLine("- message --------------------------------------").AppendLine()
                .AppendLine(message).AppendLine()
                .AppendLine("= exception ====================================").AppendLine()
                .AppendLine(ex == null ? "<NULL>" : ex.ToString())
                .AppendLine("---");

            return sb.ToString();
        }
    }
}