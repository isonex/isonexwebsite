using MaxMind.GeoIP2.Responses;
using WebApi.Services;

namespace isonexwebsite.Services
{
    public interface IGeoIpService
    {
        IsonexCountryResponse GetCountryFromIp(string ipAddress);
    }
}