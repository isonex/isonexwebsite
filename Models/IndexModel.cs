using System.Collections.Generic;
using IdentityDemo.Models.SubscriptionViewModels;
using isonexwebsite.Data.Entities;
using isonexwebsite.Models.ContactModels;

namespace isonexwebsite.Models
{
    public class IndexModel
    {
        public int SecondsTillTokenSaleStart { get; set; }
        public ContactDto Contact { get; set; }
        public SubscriptionViewModel Subscription { get; set; }

        public List<TeamMember> TeamMembers { get; set; }
    }
}