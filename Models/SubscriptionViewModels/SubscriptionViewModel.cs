﻿using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IdentityDemo.Models.SubscriptionViewModels
{
    public class SubscriptionViewModel
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public bool IsTrue => true;

        [Required]
        [Display(Name = "I agree to the terms and conditions")]
        [Compare("IsTrue", ErrorMessage = "Please agree to Terms and Conditions")]
        public bool IAgree { get; set; }

        public override string ToString() {
            var sb = new StringBuilder();
            sb.Append("FullName=").Append(FullName).Append(", Email=").Append(Email).Append(", IAgree=").Append(IAgree);
            return sb.ToString();
        }
    }
}
