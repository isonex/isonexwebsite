﻿namespace IdentityDemo.Models.AccountViewModels
{
    public class ProfileViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
