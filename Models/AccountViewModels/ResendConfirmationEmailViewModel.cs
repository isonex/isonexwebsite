﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IdentityDemo.Models.AccountViewModels
{
    public class ResendConfirmationEmailViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
