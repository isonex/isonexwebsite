﻿using System.ComponentModel.DataAnnotations;

namespace IdentityDemo.Models.AccountViewModels
{
    public class VerifyViewModel
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public int DobDay { get; set; }
        public int DobMonth { get; set; }
        public int DobYear { get; set; }

        [Required]
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Required]
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}
