﻿using Microsoft.AspNetCore.Identity;
using System;

namespace IdentityDemo.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public int KycStatus { get; set; }
        public DateTime? DateAcceptedTermsAndConditions { get; set; }

        public string MailTrainId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsTeamMember { get; set; }

        public DateTime Created { get; set; }
    }

    public enum KycStatus
    {
        NotSubmitted,
        PendingVerification,
        Verified
    }
}
