using System.ComponentModel.DataAnnotations;

namespace isonexwebsite.Models.ContactModels
{
    public class ContactDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Message { get; set; }
    }
}