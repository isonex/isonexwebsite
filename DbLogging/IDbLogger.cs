﻿using System;
using System.Threading.Tasks;

namespace IdentityDemo.DbLogging
{
    public interface IDbLogger
    {
        Guid GetNewCorrelationId();
        Task Log(string text, string type, DbLogLevel level = DbLogLevel.Info, Guid? correlationId = null);
        Task Log(string text, string type, Guid correlationId, DbLogLevel level = DbLogLevel.Info);
        Task<Guid> LogWithNewCorrelationId(string text, string type, DbLogLevel level = DbLogLevel.Info);
    }
}