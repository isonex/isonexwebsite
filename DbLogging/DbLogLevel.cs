﻿namespace IdentityDemo.DbLogging
{
    public enum DbLogLevel
    {
        Info = 1,
        Error = 40,
        Debug = 50
    }
}