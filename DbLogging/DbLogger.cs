﻿using IdentityDemo.Data;
using IdentityDemo.Data.Entities;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IdentityDemo.DbLogging
{
    public class DbLogger : IDbLogger
    {
        private readonly string connectionString;

        public DbLogger(string connectionString)
        { 
            this.connectionString = connectionString;
        }
        public async Task Log(string text, string type, DbLogLevel level = DbLogLevel.Info, Guid? correlationId = null)
        {
            var logEntry = new DbLogEntry { EntryLevel = (int)level, EntryType = type, Text = text, CorrelationId = correlationId };
            try
            {
                using(var cn = new SqlConnection(connectionString))
                {
                    cn.Open();
                    var command = new SqlCommand(
                        @"INSERT INTO [isonexb].[DbLogEntries] (EntryLevel, EntryType, Text, CorrelationId)
                                                       VALUES  (@entrylevel, @entrytype, @text, @correlationid)", cn);
                    command.CommandType = System.Data.CommandType.Text;
                    command.Parameters.AddWithValue("entrylevel", logEntry.EntryLevel);
                    command.Parameters.AddWithValue("entrytype", logEntry.EntryType);
                    command.Parameters.AddWithValue("text", logEntry.Text);
                    command.Parameters.AddWithValue("correlationid", logEntry.CorrelationId);

                    command.ExecuteNonQuery();
                }
            }
            catch(Exception ex)
            {
                // swallow
            }
        }

        public async Task Log(string text, string type, Guid correlationId, DbLogLevel level = DbLogLevel.Info)
        {
            await Log(text, type, level, correlationId);
        }

        public async Task<Guid> LogWithNewCorrelationId(string text, string type, DbLogLevel level = DbLogLevel.Info)
        {
            var correlationId = GetNewCorrelationId();

            await Log(text, type, level, correlationId);

            return correlationId;
        }

        public Guid GetNewCorrelationId()
        {
            var correlationId = Guid.NewGuid();
            return correlationId;
        }
        
    }
}
