﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace IsonexWebsite.TagHelpers
{
    // You may need to install the Microsoft.AspNetCore.Razor.Runtime package into your project
    //[HtmlTargetElement("tag-name")]
    public class EtherscanAddressTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var address = output.GetChildContentAsync().Result.GetContent();
            if (!string.IsNullOrEmpty(address))
            {
                output.Attributes.SetAttribute("href", $"https://etherscan.io/address/{address}");
                output.Attributes.SetAttribute("target", "_blank");

                var shortAddress = $"{address.Substring(0, 6)}...{address.Substring(address.Length-2, 2)}";
                output.Content.SetContent(shortAddress);
                output.TagName = "a";
            }
        }
    }
}
