# README #

Isonex Capital Website

### What is this repository for? ###

* Isonex Capital website - the final version; this is front-end and backend as one project

### How do I get set up? ###

* Use Visual Studio Code to open
* Let Visual Studio Code run "dotnet restore" when prompted
* Use F5 to run the project
* 
* If css or images seem to be missing - run "yarn" from command line (if you don't have yarn, run "npm i -g yarn")

