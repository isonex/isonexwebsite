﻿using IdentityDemo.Models.SubscriptionViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using isonexwebsite.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using isonexwebsite.Services;
using isonexwebsite.Helpers;
using IdentityDemo.Services;
using System;
using isonexwebsite.Services.MailChimp;

namespace IdentityDemo.Controllers
{
    [Route("Subscription")]
    public class SubscriptionController : Controller
    {
        private readonly ISubscriptionsService subscriptionsService;
        private readonly IRequestSerializer requestSerializer;
        private readonly IGeoIpService geoIpService;
        private readonly IEmailService emailService;
        private readonly IMailTrainService mailTrainService;
        private readonly ILogger<ContactController> logger;

        public SubscriptionController(ISubscriptionsService subscriptionsService,   
                                 IRequestSerializer requestSerializer,
                                 IGeoIpService geoIpService,
                                 IEmailService emailService,
                                 IMailTrainService mailChimpService,
                                 ILogger<ContactController> logger
                                 )
        {
            this.subscriptionsService = subscriptionsService;
            this.requestSerializer = requestSerializer;
            this.geoIpService = geoIpService;
            this.emailService = emailService;
            this.mailTrainService = mailChimpService;
            this.logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("Subscribe")]
        public IActionResult Subscribe(SubscriptionViewModel subscriptionViewModel)
        {
            try {
                logger.LogInformation("*SUBSCRIBE: " + subscriptionViewModel);

                if (ModelState.IsValid) {
                    string remoteUserIp =  Request.HttpContext.Connection.RemoteIpAddress != null ?  Request.HttpContext.Connection.RemoteIpAddress.ToString() : "";

                    string mailTrainId;
                    try {
                        mailTrainId = mailTrainService.AddSubscriberToNewsletterList(new MailTrainAddMemberToListRequest {
                            EmailAddress = subscriptionViewModel.Email,
                            LastName = subscriptionViewModel.FullName
                        });

                        }
                    catch(Exception ex) {
                        var guid = Guid.NewGuid();
                        logger.LogError(guid + ex.ToString());
                        mailTrainId = "MAILTRAIN ERROR " + guid + ": " + ex.Message;
                    }
                    
                    // even if there was an error with MailChimp, save in our database
                    Subscription subscription = new Subscription() {
                        Name = subscriptionViewModel.FullName,
                        Email = subscriptionViewModel.Email,
                        RemoteUserIp = remoteUserIp,
                        RequestJson = requestSerializer.SerializeRequest(Request),
                        MailTrainId = mailTrainId
                    };
                    
                    var isonexCountryResponse = geoIpService.GetCountryFromIp(remoteUserIp);
                    if (isonexCountryResponse != null) {
                        subscription.CountryName = isonexCountryResponse.Name;
                        subscription.CountryCode = isonexCountryResponse.IsoCode;
                    }

                    subscriptionsService.Subscribe(subscription);     
                    
                    return Ok(new { });     
                }
                else {
                    throw new Exception("Invalid request");
                }
            }
            catch(Exception ex) {
                logger.LogError(ex.ToString());
                return StatusCode(500);
            }

        }
    }
}
