﻿using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using IdentityDemo.Models;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using IdentityDemo.Data.Entities;
using System.Numerics;
using WebApi.Services;
using isonexwebsite.Models.ContactModels;
using isonexwebsite.Services;
using AutoMapper;
using isonexwebsite.Data.Entities;
using isonexwebsite.Helpers;
using Microsoft.Extensions.Logging;

namespace IdentityDemo.Controllers
{
    [Route("Contact")]
    public class ContactController : Controller
    {
        private readonly IContactService contactService;
        private readonly IRequestSerializer requestSerializer;
        private readonly IGeoIpService geoIpService;
        private readonly IEmailService emailService;
        private readonly ILogger<ContactController> logger;
        private readonly IMapper mapper;

        public ContactController(IContactService contactService,   
                                 IMapper mapper,
                                 IRequestSerializer requestSerializer,
                                 IGeoIpService geoIpService,
                                 IEmailService emailService,
                                 ILogger<ContactController> logger
                                 )
        {
            this.contactService = contactService;
            this.requestSerializer = requestSerializer;
            this.geoIpService = geoIpService;
            this.emailService = emailService;
            this.logger = logger;
            this.mapper = mapper;
        }

        [AllowAnonymous]
        [HttpPost("store")]
        public IActionResult StoreContact(ContactDto contactDto)
        {
            try {
                if (ModelState.IsValid) {
                    string remoteUserIp =  Request.HttpContext.Connection.RemoteIpAddress != null ?  Request.HttpContext.Connection.RemoteIpAddress.ToString() : "";

                    Contact contact = mapper.Map<Contact>(contactDto);
                    contact.RemoteUserIp = remoteUserIp;
                    contact.RequestJson = requestSerializer.SerializeRequest(Request);
                    
                    var countryResponse = geoIpService.GetCountryFromIp(remoteUserIp);
                    if (countryResponse != null) {
                        contact.CountryName = countryResponse.Name;
                        contact.CountryCode = countryResponse.IsoCode;
                    }

                    contactService.StoreContact(contact);     

                    emailService.SendNotification(contact);
                    
                    return Ok(new { });     
                }
                else {
                    throw new Exception("Invalid request");
                }
            }
            catch(Exception ex) {
                logger.LogError(ex.ToString());
                return StatusCode(500);
            }

        }
    }
}
