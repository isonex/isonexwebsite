﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using IdentityDemo.DbLogging;
using Microsoft.Extensions.Logging;

namespace IdentityDemo.Controllers
{
    [Route("[controller]/[action]")]
    public class DocumentsController : Controller
    {
        private readonly IDbLogger dbLogger;

        public ILogger<DocumentsController> Logger { get; }

        public DocumentsController(IDbLogger dbLogger, ILogger<DocumentsController> logger)
        {
            this.dbLogger = dbLogger;
            Logger = logger;
        }
        public async Task<IActionResult> Isonex_Whitepaper()
        {
            var correlationId = await dbLogger.LogWithNewCorrelationId("Isonex_Whitepaper download", "download");
            try
            {
                var memory = new MemoryStream();
                var path = "wwwroot/documents/Isonex Capital - IX15 White Paper.pdf";
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/pdf", Path.GetFileName(path));

            }
            catch (System.Exception exc)
            {
                Logger.LogError($"Isonex_Whitepaper download: {exc}");
                await dbLogger.Log($"Isonex_Whitepaper download error: {exc}", "download", correlationId, DbLogLevel.Error);
                return Content(exc.Message);
            }
            
        }
    

        public async Task<IActionResult> Isonex_Litepaper()
        {
            var correlationId = await dbLogger.LogWithNewCorrelationId("Isonex_Litepaper download", "download");
            try
            {
                var memory = new MemoryStream();
                var path = "wwwroot/documents/Isonex Capital - IX15 Lite Paper.pdf";
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/pdf", Path.GetFileName(path));

            }
            catch (System.Exception exc)
            {
                Logger.LogError($"Isonex_Litepaper download: {exc}");
                await dbLogger.Log($"Isonex_Litepaper download error: {exc}", "download", correlationId, DbLogLevel.Error);
                return Content(exc.Message);
            }
            
        }

        public async Task<IActionResult> Isonex_SlideDeck()
        {
            var correlationId = await dbLogger.LogWithNewCorrelationId("Isonex_SlideDeck download", "download");
            try
            {
                var memory = new MemoryStream();
                var path = "wwwroot/documents/Isonex Capital IX15 Slide Deck.pdf";
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/pdf", Path.GetFileName(path));

            }
            catch (System.Exception exc)
            {
                Logger.LogError($"Isonex_SlideDeck download: {exc}");
                await dbLogger.Log($"Isonex_SlideDeck download error: {exc}", "download", correlationId, DbLogLevel.Error);
                return Content(exc.Message);
            }

        }
    }
}