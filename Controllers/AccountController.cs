﻿using System.ComponentModel.DataAnnotations;
using IdentityDemo.Models.AccountViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using IdentityDemo.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using isonexwebsite.Services;
using System.Threading.Tasks;
using IdentityDemo.Services;
using IdentityDemo.Models;
using System.Linq;
using System;
using isonexwebsite.Services.MailChimp;
using isonexwebsite.Data.Entities;
using isonexwebsite.Helpers;
using IdentityDemo.DbLogging;

namespace IdentityDemo.Controllers
{
    [Route("account")]
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ISubscriptionsService subscriptionsService;
        private readonly IRequestSerializer requestSerializer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IBlockchainService _blockchainService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger logger;
        private readonly IDbLogger dbLogger;
        private readonly IEmailService _emailService;
        private readonly IIdentityVerification _identityVerification;
        private readonly IMailTrainService mailTrainService;
        private readonly IPriceUdateRepository _priceUdateRepository;
        private readonly IEthereumPriceHistory _ethereumPriceHistory;
        private readonly IGeoIpService geoIpService;

        public AccountController(UserManager<ApplicationUser> userManager,
            ILogger<AccountController> logger,
            IDbLogger dbLogger,
            IEmailService emailService,
            SignInManager<ApplicationUser> signInManager,
            IBlockchainService blockchainService,
            IIdentityVerification identityVerification,
            IMailTrainService mailTrainService,
            IPriceUdateRepository priceUdateRepository,
            IEthereumPriceHistory ethereumPriceHistory,
            ISubscriptionsService subscriptionsService,
            IRequestSerializer requestSerializer,
            IGeoIpService geoIpService
            )
        {
            this.mailTrainService = mailTrainService;
            _signInManager = signInManager;
            _blockchainService = blockchainService;
            _userManager = userManager;
            this.logger = logger;
            this.dbLogger = dbLogger;
            _emailService = emailService;
            _identityVerification = identityVerification;
            _priceUdateRepository = priceUdateRepository;
            _ethereumPriceHistory = ethereumPriceHistory;
            this.subscriptionsService = subscriptionsService;
            this.requestSerializer = requestSerializer;
            this.geoIpService = geoIpService;
        }

        [Route("acceptterms")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AcceptTerms()
        {
            ApplicationUser user = await GetUser();

            if (user.DateAcceptedTermsAndConditions == null)
            {
                user.DateAcceptedTermsAndConditions = DateTime.Now;
            }

            await _userManager.UpdateAsync(user);

            return RedirectToActionPermanent("deposit");
        }

        [Route("verification")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Verification(VerifyViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await GetUser();

                model.CountryName = ShuftiProCountries.GetCountryName(model.CountryCode);
                //var result = await _identityVerification.VerifyAsync(user.Email, model);
                //if (result)
                {
                    logger.LogInformation("Verifying user");

                    user.KycStatus = (int)IdentityDemo.Models.KycStatus.Verified;

                    await _userManager.UpdateAsync(user);

                    return View("verification-submitted");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [Route("verification")]
        public async Task<IActionResult> Verification()
        {
            ApplicationUser user = await GetUser();
            SetViewBagProperties(user);

            if (user.KycStatus == (int) IdentityDemo.Models.KycStatus.Verified)
            {
                return View("verification-complete");
            }
            else if (user.KycStatus == (int)IdentityDemo.Models.KycStatus.PendingVerification)
            {
                //var verificationResult = await _identityVerification.CheckVerificationStatusAsync(user.KYCPendingVerification.ToString());
                //var verificationResult = await _identityVerification.CheckVerificationStatusAsync("daf7842bee7747a69bd840450e1dbb93");


                //if (verificationResult == VerificationStatus.IsPending)
                //{
                return View("verification-submitted");
                //}
                //else if (verificationResult == VerificationStatus.IsVerified)
                //{
                //    return View("verification-complete");
                //}
                //else if (verificationResult == VerificationStatus.FailedVerification)
                //{
                //    return View("verification-rejected");
                //}
            }

            return View();
        }

        [Route("profile")]
        public async Task<IActionResult> Profile()
        {
            ApplicationUser user = await GetUser();
            SetViewBagProperties(user);

            if (user == null)
            {
                return RedirectToLocal("Login");
            }

            var profileViewModel = new ProfileViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName
            };
            return View("profile", profileViewModel);
         }

        [Route("profile")]
        [HttpPost]
        public async Task<IActionResult> Profile(ProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await GetUser();
                SetViewBagProperties(user);

                if (user == null)
                {
                    return RedirectToLocal("Login");
                }

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;

                await _userManager.UpdateAsync(user);

                return View("profile", model);
            }

            return View(model);
        }

        [Route("deposit")]
        public async Task<IActionResult> Deposit()
        {
            ApplicationUser user = await GetUser();
            SetViewBagProperties(user);

            var ethereumDepositingAddress = _blockchainService.GetUserEthereumAddresses(user.Id).FirstOrDefault()?.Address;

            var model = new DepositViewModel();

            model.IsVerified = user.KycStatus == (int)IdentityDemo.Models.KycStatus.Verified;
            model.VerificationPending = user.KycStatus == (int)IdentityDemo.Models.KycStatus.PendingVerification;
            model.AcceptedTerms = user.DateAcceptedTermsAndConditions != null;
            model.EthereumDepositingAddress = ethereumDepositingAddress;
            model.PersonalEthereumAddress = _blockchainService.GetUserPersonalEthereumAddresses(user.Id).FirstOrDefault()?.Address;

            return View(model);
        }

        [Route("my-tokens")]
        public async Task<IActionResult> MyTokens()
        {
            ApplicationUser user = await GetUser();
            SetViewBagProperties(user);

            var personalEthereumAddresses = _blockchainService.GetUserPersonalEthereumAddresses(user.Id);
            var participantEthereumTransactions = _blockchainService.GetMyEthereumDepositStatuses(user.Id);

            var model = new MyTokensViewModel();

            model.PersonalEthereumAddresses = personalEthereumAddresses;
            model.ParticipantEthereumTransactions = participantEthereumTransactions;

            return View("my-tokens", model);
        }

        [Route("ix-team")]
        public async Task<IActionResult> IxTeam()
        {
            ApplicationUser user = await GetUser();

            SetViewBagProperties(user);

            if (!user.IsTeamMember)
            {
                throw new Exception("Invalid Request");
            }

            var personalEthereumAddresses = _blockchainService.GetUserPersonalEthereumAddresses(user.Id);

            var model = new IxTeamMember()
            {
                EthereumAccount = personalEthereumAddresses.Any() ? personalEthereumAddresses.First().Address : string.Empty
            };

            return View("ix-team", model);
        }

        [Route("ix-team")]
        [HttpPost]
        public async Task<IActionResult> AddTeamEthAddress(IxTeamMember model)
        {
            ApplicationUser user = await GetUser();

            if (!user.IsTeamMember)
            {
                throw new Exception("Invalid Request");
            }

            SetViewBagProperties(user);

            _blockchainService.SetUserPersonalEthereumAddresses(user.Id, model.EthereumAccount);

            return View("ix-team", model);
        }

        [Route("set-personal-address")]
        [HttpPost]
        public async Task<IActionResult> SetPersonalAddress(DepositViewModel model)
        {
            ApplicationUser user = await GetUser();

            var personalEthereumAddresses = _blockchainService.GetUserPersonalEthereumAddresses(user.Id);

            if (string.IsNullOrEmpty(model.PersonalEthereumAddress) || personalEthereumAddresses.Any())
            {
                throw new Exception("Invalid Request");
            }

            // make sure its a valid ethereum address

            SetViewBagProperties(user);

            _blockchainService.SetUserPersonalEthereumAddresses(user.Id, model.PersonalEthereumAddress);

            return RedirectToAction("Deposit");
        }

        private async Task<ApplicationUser> GetUser()
        {
            var userId = HttpContext.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value;

            var user = _userManager.Users.FirstOrDefault(u => u.Id == userId);

            return user;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ShowResend"] = false;
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("register")]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("register")]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            var correlationId = await dbLogger.LogWithNewCorrelationId($"Register {model}", "register");

            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    logger.LogInformation("User created a new account with password.");

                    _blockchainService.AssignDepositingAddresses(user.Id);

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                    _emailService.SendEmailConfirmation(model.Email, callbackUrl);

                    string remoteUserIp = Request.HttpContext.Connection.RemoteIpAddress != null ? Request.HttpContext.Connection.RemoteIpAddress.ToString() : "";

                    string mailTrainId;
                    try {
                        mailTrainId = mailTrainService.AddAccountToList(new MailTrainAddMemberToListRequest {
                            EmailAddress = model.Email,
                        });
                    }
                    catch(Exception ex) {
                        logger.LogError(correlationId + ex.ToString());
                        await dbLogger.Log($"Register {model} mailtrain AddAccountToList fail: {ex}", "register", correlationId);
                        mailTrainId = "MAILTRAIN ERROR " + correlationId + ": " + ex.Message;
                    }

                    user.MailTrainId = mailTrainId;

                    // made the below synchronous (wait for .Result) - then the update actually happens in the db; although it might have been only because
                    // I was testing on localhost and might have shut down the app before the async operation finished
                    var updateResult = _userManager.UpdateAsync(user).Result;

                    if (model.Subscribe) {
                        try
                        {
                            mailTrainId = mailTrainService.AddSubscriberToNewsletterList(new MailTrainAddMemberToListRequest
                            {
                                EmailAddress = model.Email,
                                FirstName = model.FirstName,
                                LastName = model.LastName
                            });

                        }
                        catch (Exception ex)
                        {
                            logger.LogError(correlationId + ex.ToString());
                            await dbLogger.Log($"Register {model} mailtrain AddSubscriberToNewsletterList fail: {ex}", "register", correlationId);
                            mailTrainId = "MAILTRAIN ERROR " + correlationId + ": " + ex.Message;
                        }

                        // even if there was an error with MailTrain, save in our database
                        Subscription subscription = new Subscription()
                        {
                            Name = $"{model.FirstName} {model.LastName}",
                            Email = model.Email,
                            RemoteUserIp = remoteUserIp,
                            RequestJson = requestSerializer.SerializeRequest(Request),
                            MailTrainId = mailTrainId
                        };

                        var isonexCountryResponse = geoIpService.GetCountryFromIp(remoteUserIp);
                        if (isonexCountryResponse != null)
                        {
                            subscription.CountryName = isonexCountryResponse.Name;
                            subscription.CountryCode = isonexCountryResponse.IsoCode;
                        }

                        subscriptionsService.Subscribe(subscription);
                    }

                    logger.LogInformation($"User '{model.Email}' created a new account with password. Awaiting email confirmation.");
                    await dbLogger.Log($"User '{model.Email}' created a new account with password. Awaiting email confirmation.", "register", correlationId);
                    return RedirectToAction("PleaseConfirmYourEmail");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ResendConfirmationEmail")]
        public async Task<IActionResult> ResendConfirmationEmail()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("ResendConfirmationEmail")]
        public async Task<IActionResult> ResendConfirmationEmail(ResendConfirmationEmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToAction(nameof(ResendConfirmationEmail));
                }

                if (await _userManager.IsEmailConfirmedAsync(user)) {
                    return RedirectToAction(nameof(Login));
                }

                var result = await _userManager.UpdateSecurityStampAsync(user);

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                _emailService.SendEmailConfirmation(model.Email, callbackUrl);

                return RedirectToAction(nameof(PleaseConfirmYourEmail));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("PleaseConfirmYourEmail")]
        public async Task<IActionResult> PleaseConfirmYourEmail()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);

            await dbLogger.Log($"ConfirmEmailAsync {user}: {result.Succeeded}", "register");
            
            return View(result.Succeeded ? "EmailConfirmed" : "Error");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("login")]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    logger.LogInformation("User logged in.");
                    return RedirectToLocal(returnUrl);
                }
                else if (result.IsNotAllowed)
                {
                    logger.LogInformation($"User's '{model.Email}' email address not confirmed.");
                    ModelState.AddModelError(string.Empty, "You must confirm your email address to log in.");
                    ViewData["ShowResend"] = true;
                    return View(model);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    ViewData["ShowResend"] = false;
                    return View(model);
                }
            }
            ViewData["ShowResend"] = false;
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                //if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToAction(nameof(ForgotPasswordConfirmation));
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                await _emailService.SendEmail(model.Email, "Reset Password",
                   $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>", true);
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ForgotPasswordConfirmation")]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("ResetPasswordConfirmation")]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        [Route("GetNumberOfTokensFor")]
        public dynamic GetNumberOfTokensFor(decimal ether)
        {
            if (ether <= 0)
            {
                return 0;
            }

            try
            {
                var ethPrice = _ethereumPriceHistory.GetCurrentPrice();
                var stagePrice = 0.95m;

                return new
                {
                    etherUsdPrice = ethPrice,
                    ix15UsdPrice = stagePrice,
                    amount = (ether * ethPrice) / stagePrice
                };
            }
            catch (Exception ex)
            {
                // Log it

                return new
                {
                    etherUsdPrice = 0,
                    ix15UsdPrice = 0,
                    amount = 0
                };
            }

            
        }

        private void SetViewBagProperties(ApplicationUser user)
        {
            ViewBag.IsTeamMember = user.IsTeamMember;
        }

        [Route("{view=Index}")]
        public async Task<IActionResult> Index(string view)
        {
            ApplicationUser user = await GetUser();

            SetViewBagProperties(user);

            var crixData = _priceUdateRepository.GetCrixPriceHistory(DateTime.Now.AddDays(-30), DateTime.Now);
            var model = new AccountModel()
            {
                Holdings = new List<Holding>() {
                    new Holding("BTC",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("ETH",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("XRP",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("BCH",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("EOS",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("XLM",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("LTC",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("USDT",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("ADA",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("XMR",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("MIOTA",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("DASH",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("TRX",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("NEO",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                    new Holding("ETC",1234, 2323, 12341, 1, new DateTime(2018, 10, 01)),
                },
                CrixPriceHistory = crixData
            };

            return View(view, model);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(AccountController.Index), "Account");
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }

    public class MyTokensViewModel
    {
        public double TotalPendingIX15FromEthereumTransfers => ParticipantEthereumTransactions.Where(t => !t.Ix15TokensTransferedToParticipant).Sum(t => t.DepositedAmount) / Math.Pow(10, 18);
        public double TotalDeliveredIX15FromEthereumTransfers => ParticipantEthereumTransactions.Where(t => t.Ix15TokensTransferedToParticipant).Sum(t => t.DepositedAmount) / Math.Pow(10, 18);

        public IEnumerable<PersonalEthereumAddress> PersonalEthereumAddresses { get; internal set; }
        public IEnumerable<ParticipantEthereumTransactions> ParticipantEthereumTransactions { get; internal set; }

        public IEnumerable<string> AllPersonalEthereumAddresses => PersonalEthereumAddresses.Select(p => p.Address).Concat(ParticipantEthereumTransactions.Select(t => t.FromAddress)).Distinct();
    }

    public class DepositViewModel
    {
        public bool VerificationPending { get; set; }
        public bool IsVerified { get; set; }
        public bool AcceptedTerms { get; set; }
        public string EthereumDepositingAddress { get; set; }
        public string PersonalEthereumAddress { get; set; }
    }

    public class AccountModel
    {
        public IEnumerable<Holding> Holdings { get; set; }

        public IEnumerable<PriceHistory> CrixPriceHistory { get; set; }
    }

    public class Holding
    {
        public string Coin { get; }
        public decimal Amount { get; }
        public decimal DollarValue { get; }
        public long MarketCap { get; set; }
        public int Rank { get; set; }
        public DateTime DateAddedToFund { get; set; }

        public Holding(string coin, decimal amount, decimal dollarValue, long marketCap, int rank, DateTime dateAddedToFund)
        {
            this.Coin = coin;
            this.Amount = amount;
            this.DollarValue = dollarValue;
            this.MarketCap = marketCap;
            this.Rank = rank;
            this.DateAddedToFund = dateAddedToFund;
        }
    }
}