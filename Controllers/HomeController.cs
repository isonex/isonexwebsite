﻿using IdentityDemo.Models.SubscriptionViewModels;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using IdentityDemo.Models;
using isonexwebsite.Models;
using isonexwebsite.Models.ContactModels;
using isonexwebsite.Services;
using System;
using isonexwebsite.Services.MailChimp;

namespace IdentityDemo.Controllers
{
    public class HomeController : Controller
    {
        public readonly IGlobalConfigService globalConfigService;
        private readonly IEmailService emailService;

        public HomeController(IGlobalConfigService globalConfigService,
                              IEmailService emailService
                              )
        {
            this.globalConfigService = globalConfigService;
            this.emailService = emailService;
        }

        [Route("")]
        public IActionResult Index()
        {
            IndexModel model = GetIndexModel();

            return View(model);
        }

        [Route("privacy-policy")]
        public IActionResult PrivacyPolicy()
        {
            return View();
        }

        [Route("cookies")]
        public IActionResult Cookies()
        {
            return View();
        }

        [Route("terms")]
        public IActionResult Terms()
        {
            return View();
        }

        // [Route("tezd")]
        // public IActionResult Test()
        // {
        //     var result = mailTrainService.AddSubscriberToNewsletterList(new MailTrainAddMemberToListRequest {
        //         EmailAddress = "adam@adambieganski.com",
        //         FirstName = "Adam",
        //         LastName = "Bieganski"
        //     });

        //     return null;
        // }

        private IndexModel GetIndexModel()
        {
            var config = globalConfigService.GetGlobalConfig();
            var teamMembers = globalConfigService.GetTeamMembers(true);
            var seconds = (int)(config.TokenSaleStart - DateTime.Now).TotalSeconds;

            IndexModel model = new IndexModel
            {
                SecondsTillTokenSaleStart = seconds,
                Contact = new ContactDto(),
                Subscription = new SubscriptionViewModel(),
                TeamMembers = teamMembers
            };
            return model;
        }

        // public IActionResult Robots()
        // {
        //     var content = new StringBuilder("User-agent: *" + Environment.NewLine);
        //     content.Append("Disallow: /" + Environment.NewLine);

        //     return File(
        //                 Encoding.UTF8.GetBytes(content.ToString()),
        //                 "text/plain");
        // }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
