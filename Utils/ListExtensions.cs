﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityDemo.Utils
{
    public static class ListExtensions
    {
        public static string ToCommaSeparatedString(this IList<string> list)
        {
            if (list == null || list.Count == 0) return "";
            else if (list.Count == 1) return list[0];
            else
            {
                var sb = new StringBuilder();
                foreach (var item in list)
                {
                    sb.Append(item).Append(",");
                }
                sb.Remove(sb.Length - 1, 1);
                return sb.ToString();
            }
        }
    }
}
